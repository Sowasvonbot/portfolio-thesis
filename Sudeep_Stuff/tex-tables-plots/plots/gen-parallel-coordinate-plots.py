#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.pyplot import figure

"""
indices:
    0: score
    1: CPUTime
    2: Walltime
    3: Memory
    4: Energy
"""

x_labels = [
    "Unsolved\n tasks",
    "CPU time\n (s/sp)",
    "Wall time\n (s/sp)",
    "Memory\n (MB/sp)",
    "Energy\n (J/sp)",
]


def parallel_coordinates(data_sets, min_max_range, style, markers, legend_names, filename):
    """
    This would basically plot n plots and then put them together.
    """

    # dimension, i.e., number of coordinates in parallel coordinate plot
    dims = len(data_sets[0])
    x = range(dims)
    # Create n-1 plots, we will put them together later
    fig, axes = plt.subplots(1, dims - 1, sharey=False)

    if style is None:
        style = ["r-"] * len(data_sets)

    # Normalize the data sets
    norm_data_sets = []
    for ds in data_sets:
        # We take the value of the first one, i.e., score and inverse the others
        nds = [
            (value - min_max_range[dimension][0]) / min_max_range[dimension][2]
#            if dimension == 0
#            else 1 - (value - min_max_range[dimension][0]) / min_max_range[dimension][2]
            for dimension, value in enumerate(ds)
        ]
        norm_data_sets.append(nds)
    data_sets = norm_data_sets

    # Plot the datasets on all the subplots
    for i, ax in enumerate(axes):
        for dsi, d in enumerate(data_sets):
            ax.plot(x, d, style[dsi], label=legend_names[dsi],marker=markers[dsi],fillstyle="none",  markersize=10)
            ax.set_xlim([x[i], x[i + 1]])
            ax.set_ylim(0, 1)
            ax.spines["top"].set_visible(False)
            ax.spines["bottom"].set_visible(False)

    # To set the legen position
    #plt.legend(loc="lower left", bbox_to_anchor=(1, -0.3))
    # tools plot has 8 plots
    if len(legend_names) == 8:
        yshift = -0.35
    else:
        yshift = -0.3
    plt.legend(loc='center',
               bbox_to_anchor=(-1, yshift),
               ncol=4)

    # Set the x axis ticks
    for dimension, (axx, xx) in enumerate(zip(axes, x)):
        axx.xaxis.set_major_locator(ticker.FixedLocator([xx]))
        axx.xaxis.set_major_formatter(plt.FixedFormatter([x_labels[dimension]]))
        ticks = len(axx.get_yticklabels())
        step = min_max_range[dimension][2] / (ticks - 1)
        mn = min_max_range[dimension][0]
        labels = ["%d" % (mn + i * step) for i in range(ticks)]
        # since the first one is not inverted its lables don't need to be reversed
        if dimension == 0:
            axx.set_yticklabels(labels)
        else:
            axx.set_yticklabels(labels)

    # Move the final axis' ticks to the right-hand side
    axx = plt.twinx(axes[-1])
    dimension += 1
    axx.xaxis.set_major_locator(ticker.FixedLocator([x[-2], x[-1]]))
    axx.xaxis.set_major_formatter(plt.FixedFormatter([x_labels[-2], x_labels[-1]]))
    mn = min_max_range[dimension][0]
    axx.spines["top"].set_visible(False)
    axx.spines["bottom"].set_visible(False)

    ticks = len(axx.get_yticklabels())
    step = min_max_range[dimension][2] / (ticks - 1)
    labels = ["%d" % (mn + i * step) for i in range(ticks)]
    axx.set_yticklabels(labels)

    # Stack the subplots
    plt.subplots_adjust(wspace=0)
    plt.gcf().set_size_inches(8, 2.5)
    # figure(figsize=(16, 9), dpi=80)
    fig.savefig(filename, bbox_inches="tight")

    return plt


if __name__ == "__main__":
    data_cpa = [3223, 77, 55, 780, 850]

    # For standalone tools
    data_esbmc = [4373, 31, 31, 270, 380]
    data_symbiotic = [5880, 16, 16, 160, 210]
    data_uautomizer = [4523, 49, 30, 600, 490]
    data_cbmc = [5380, 24, 24, 280, 280]
    data_divine = [5920, 59, 14, 540, 420]
    data_goblint = [7498, 15, 15, 42, 200]
    data_utaipan = [5134, 55, 35, 500, 560]
    data = [
        data_cpa,
        data_esbmc,
        data_symbiotic,
        data_uautomizer,
        data_cbmc,
        data_divine,
        data_goblint,
        data_utaipan,
    ]
    colors = ["r", "b", "g", "cyan", "olive", "magenta", "brown", "navy"]
    markers = ["x", "o", "s", "+", "^", "v", "p", "D"]    
    legend_names = [
        "CPAchecker",
        "ESBMC",
        "Symbiotic",
        "UAutomizer",
        "CBMC",
        "DIVINE",
        "GOBLINT",
        "UTaipan",
    ]

    min_max_range_full = [
        (0, 7500, 7500),
        (0, 150, 150),
        (0, 150, 150),
        (0, 1500, 1500),
        (0, 1500, 1500),
    ]

    parallel_coordinates(data, min_max_range_full, colors, markers, legend_names, "pc-tools.pdf")

    # Common min_max_range for compositions

    # For sequence

    data_seq_best = [2581, 90, 72, 890, 1100]
    data_seq_worst = [2738, 82, 64, 920, 950]
    data = [data_cpa, data_seq_best, data_seq_worst]
    colors = ["b", "r", "g"]
    markers = ["o", "x", "s"]
    legend_names = ["CPAchecker", "SeqPortfolio-4", "SeqPortfolio-8"]
    parallel_coordinates(data, min_max_range_full, colors, markers, legend_names, "pc-seq.pdf")

    # For Parallel
    data_par_best = [2485, 66, 23, 1200, 550]
    data_par_worst = [3474, 130, 31, 1400, 850]
    data = [data_cpa, data_par_best, data_par_worst]
    colors = ["b", "r", "g"]
    markers = ["o", "x", "s"]
    legend_names = ["CPAchecker", "ParPortfolio-3", "ParPortfolio-8"]
    parallel_coordinates(data, min_max_range_full, colors, markers, legend_names, "pc-par.pdf")

    data_sel_best = [2658, 76, 63, 600, 910]
    data_sel_worst = [2964, 77, 61, 750, 890]
    data = [data_cpa, data_sel_best, data_sel_worst]
    colors = ["b", "r", "g"]
    markers = ["o", "x", "s"]
    legend_names = ["CPAchecker", "AlgoSelection-8", "AlgoSelection-2"]
    parallel_coordinates(data, min_max_range_full, colors, markers, legend_names, "pc-sel.pdf")
