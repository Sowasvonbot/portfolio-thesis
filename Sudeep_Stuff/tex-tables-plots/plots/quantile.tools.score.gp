
set output 'quantile.tools.score.pdf'
set terminal pdf font "Helvetica,10"

set tmargin 1.2
set bmargin 0
set lmargin 7
set rmargin 3
unset xlabel
unset xtics
set ylabel 'Min. time in s' offset 3
set key left top
set logscale y 10
set pointsize 1.0
set multiplot layout 2,1
# set size 1,0.86
set origin 0,0.150

set xrange [-2000:10000]
set yrange [1:1000]

set style data linespoints

plot \
     "../plot-data/cpa-seq.quantile.score.csv" using 1:5 title "CPAchecker" with linespoints linecolor rgb "red" pointtype 2 pointinterval -500, \
     "../plot-data/esbmc.quantile.score.csv" using 1:5 title "ESBMC" with linespoints linecolor rgb "blue" pointtype 6 pointinterval -500, \
     "../plot-data/symbiotic.quantile.score.csv" using 1:5 title "Symbiotic" with linespoints linecolor rgb "dark-green" pointtype 4 pointinterval -500, \
     "../plot-data/uautomizer.quantile.score.csv" using 1:5 title "UAutomizer" with linespoints linecolor rgb "cyan" pointtype 1 pointinterval -500, \
     "../plot-data/cbmc.quantile.score.csv" using 1:5 title "CBMC" with linespoints linecolor rgb "olive" pointtype 8 pointinterval -500, \
     "../plot-data/divine.quantile.score.csv" using 1:5 title "DIVINE" with linespoints linecolor rgb "magenta" pointtype 10 pointinterval -500, \
     "../plot-data/goblint.quantile.score.csv" using 1:5 title "Goblint" with linespoints linecolor rgb "brown" pointtype 14 pointinterval -500, \
     "../plot-data/utaipan.quantile.score.csv" using 1:5 title "UTaipan" with linespoints linecolor rgb "navy" pointtype 12 pointinterval -500;

unset logscale
set yrange [0:1]
unset key
unset bmargin
set tmargin 0
set xtics nomirror
unset ytics
unset ylabel
set size 1,0.14
set origin 0,0.01
set xlabel 'Cumulative score'

plot \
     "../plot-data/cpa-seq.quantile.score.csv" using 1:5 title "CPAchecker" with linespoints linecolor rgb "red" pointtype 2 pointinterval -500, \
     "../plot-data/esbmc.quantile.score.csv" using 1:5 title "ESBMC" with linespoints linecolor rgb "blue" pointtype 6 pointinterval -500, \
     "../plot-data/symbiotic.quantile.score.csv" using 1:5 title "Symbiotic" with linespoints linecolor rgb "dark-green" pointtype 4 pointinterval -500, \
     "../plot-data/uautomizer.quantile.score.csv" using 1:5 title "UAutomizer" with linespoints linecolor rgb "cyan" pointtype 1 pointinterval -500, \
     "../plot-data/cbmc.quantile.score.csv" using 1:5 title "CBMC" with linespoints linecolor rgb "olive" pointtype 8 pointinterval -500, \
     "../plot-data/divine.quantile.score.csv" using 1:5 title "DIVINE" with linespoints linecolor rgb "magenta" pointtype 10 pointinterval -500, \
     "../plot-data/goblint.quantile.score.csv" using 1:5 title "Goblint" with linespoints linecolor rgb "brown" pointtype 14 pointinterval -500, \
     "../plot-data/utaipan.quantile.score.csv" using 1:5 title "UTaipan" with linespoints linecolor rgb "navy" pointtype 12 pointinterval -500;
