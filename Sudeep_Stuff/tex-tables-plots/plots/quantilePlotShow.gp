
set terminal svg font "Helvetica,10" size 800,400
set output 'quantilePlot-unreach-call-ReachSafety-Arrays.svg'
set tmargin 1.2
set bmargin 0
set lmargin 7
set rmargin 2.3
unset xlabel
unset xtics
set ylabel 'Min. time in s' offset 3
set key left top
set logscale y 10
set pointsize 1.0
set multiplot layout 2,1
set size 1,0.86
set origin 0,0.150

set xrange [-100:160]
set yrange [1:1000]
plot  'results-qplots/QPLOT.unreach-call-ReachSafety-Arrays.CPA-Par.quantile-plot.csv' using 1:2 with linespoints linecolor rgb "olive" pointtype 12 pointinterval 100 linewidth 2 title 'CPA-Par', 'results-qplots/QPLOT.unreach-call-ReachSafety-Arrays.CPA-Seq.quantile-plot.csv' using 1:2 with linespoints linecolor rgb "blue" pointtype 3 pointinterval 100 linewidth 2 title 'CPA-Seq', 'results-qplots/QPLOT.unreach-call-ReachSafety-Arrays.ESBMC-Par.quantile-plot.csv' using 1:2 with linespoints linecolor rgb "orange" pointtype 6 pointinterval 100 linewidth 2 title 'ESBMC-Par', 'results-qplots/QPLOT.unreach-call-ReachSafety-Arrays.ESBMC.quantile-plot.csv' using 1:2 with linespoints linecolor rgb "red" pointtype 4 pointinterval 100 linewidth 2 title 'ESBMC';

unset logscale
set yrange [0:1]
unset key
unset bmargin
set tmargin 0
set xtics nomirror
unset ytics
unset ylabel
set size 1,0.14
set origin 0,0.01
set xlabel 'Cumulative score'

plot  'results-qplots/QPLOT.unreach-call-ReachSafety-Arrays.CPA-Par.quantile-plot.csv' using 1:2 with linespoints linecolor rgb "olive" pointtype 12 pointinterval 100 linewidth 2 title 'CPA-Par', 'results-qplots/QPLOT.unreach-call-ReachSafety-Arrays.CPA-Seq.quantile-plot.csv' using 1:2 with linespoints linecolor rgb "blue" pointtype 3 pointinterval 100 linewidth 2 title 'CPA-Seq', 'results-qplots/QPLOT.unreach-call-ReachSafety-Arrays.ESBMC-Par.quantile-plot.csv' using 1:2 with linespoints linecolor rgb "orange" pointtype 6 pointinterval 100 linewidth 2 title 'ESBMC-Par', 'results-qplots/QPLOT.unreach-call-ReachSafety-Arrays.ESBMC.quantile-plot.csv' using 1:2 with linespoints linecolor rgb "red" pointtype 4 pointinterval 100 linewidth 2 title 'ESBMC';
