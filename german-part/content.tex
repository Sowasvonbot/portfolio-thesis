\section{Einführung}
Automatische Software Verifikation ist ein komplexer und rechenintensiver Zweig der Informatik.
Daher werden für die Berechnungen solcher Verifizierungen viele verschiedene Algorithmen und Heuristiken eingesetzt.
Deshalb existieren viele unterschiedliche Software-Verifizierungsprogramme (im Nachfolgenden Software Verifizierer genannt) mit unterschiedlichen Stärken und Schwächen. 
Dies kann dazu führen, dass ein Software Verifizier ein gegebenes Problem innerhalb von Sekunden lösen kann, während ein anderer Verifizierer mehrere Minuten oder gar Stunden dafür benötigt. 
Deswegen kann es sehr sinnvoll sein, mehrere solcher Verifizierer zu kombinieren, um deren Stärken nutzen zu können.
\\[2ex]
Wir haben eine solche Komposition von Verifizierern, das \portfolio, entwickelt. 
Ein \portfolio nimmt eine beliebige Menge an Software Verifizierern und gibt allen dasselbe Programm und dieselbe Spezifikation zur Überprüfung. 
Als Ergebnis liefert es das am schnellsten produzierte und valide Ergebnis eines dieser Verifizierers zurück.
Die Komposition haben wir in dem Programm \coveriteam \cite{coveriteam-paper} implementiert.
\coveriteam stellt eine eigene Sprache zur Verfügung mit deren Hilfe Verifizierungsprogramme in vordefinierten Komposition verknüpft werden können.

\begin{algorithm}[t]
	\caption{\portfolio}
	\label{alg:portfolio-example}
	\algorithmicrequire \quad Programm p, Spezifikation s\\
	\algorithmicensure \quad Urteil
	\begin{algorithmic}[1]
		\STATE cpa_checker $\coloneqq$ Verifizierer(\dq CPAChecker\dq)
		\STATE esbmc $\coloneqq$ Verifizierer(\dq ESBMC\dq)
		\STATE stop_bedingung $\coloneqq$ urteil $\in \{\textit{TRUE},\,\textit{FALSE}\}$
		\STATE parallel_portfolio $\coloneqq$ ParallelPortfolio(cpa_checker, esbmc, stop_bedingung)
		\STATE ergebnis $\coloneqq$ parallel_portfolio.verifiziere(p,s)
		\RETURN (ergebnis.urteil,\,ergebnis.zeugnis)
	\end{algorithmic}
\end{algorithm}

\section{Idee}
Die Grundidee des \portfolio ist die Stärken mehrerer Software Verifizierer dafür zu nutzen.
Außerdem wollen wir ein möglichst schnelles Ergebnis zu bekommen.
Als Beispiel hierfür haben wir zwei Programme aus dem Software-Verifizierungswettbewerb \svcomp 22 \cite{SVCOMP22} gefunden, die gegen die \texttt{unreach-call}-Spezifikation getestet wurden.
Das erste Programm \glqq 
\texttt{while_infinite_loop_3.c}
\grqq
\footnote{\url{
		https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/loops/while_infinite_loop_3.c
}} 
wurde von dem Verifizierer \cpachecker\cite{CPACHECKER} innerhalb von 5 Sekunden gelöst, während der Verifizier \esbmc\cite{ESBMC-kind} es nicht in der vorgegebenen CPU-Zeit lösen konnte. 
Das zweite Programm
\glqq
\texttt{sanfoundry_10_ground.c}
\grqq
\footnote{\url{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/array-examples/sanfoundry_10_ground.c}}
konnte diesmal nicht von \cpachecker gelöst werden. 
\esbmc hingegen benötigte nicht mal eine Sekunde dafür.
Unser \portfolio, für diesen Fall in \cref{alg:portfolio-example} dargestellt, kombiniert beide Verifizierer und sollte somit in der Lage sein beide Programme zu verifizieren.

\section{Umsetzung}
\subsection{Sprache}
\begin{lstlisting}[language=ANTLR,tabsize=2,caption=Definition der Kompositionen,label=lst:actor-defs]
	|	'SEQUENCE' '('actor ',' actor ')'
	|	'ITE' '(' exp ',' actor (',' actor)? ')'	
	|	'REPEAT' '(' quoted_ID ',' actor ')'
	|	'PARALLEL' '(' actor ',' actor ')'
	|	'PARALLEL_PORTFOLIO' '('  actor (',' actor )* (',' exp)')'
\end{lstlisting}
Für die Implementierung der \portfolio -Komposition haben wir das Programm \coveriteam gewählt.
\coveriteam ermöglicht die Komposition verschiedenster Software-Verifizierungsprogramme mittels einer eigenen Sprache.
Wir haben diese Sprache um eine weitere Komposition erweitert.
\cref{lst:actor-defs} zeigt den Ausschnitt der Sprache, der die Kompositionen definiert.
Die einzelnen Komposition funktionieren dabei wie folgt:
\begin{itemize}
    \item \texttt{SEQUENCE}: Eine Sequenz aus zwei Aktoren, welche nacheinander ausgeführt werden.
    \item \texttt{ITE}: Steht für \glqq if-then-else\grqq, also eine wenn-dann-Komposition. 
    Sie kann mit einem Aktor oder mit zwei Aktoren benutzt werden. 
    \glqq exp\grqq steht hierbei für eine Bedingung (\engl expression). 
    Ist nur ein Aktor gegeben, wird die Bedingung überprüft und falls diese erfüllt ist, wird dieser ausgeführt.
    Sind zwei Aktoren gegeben, dann wird der Erste ausgeführt, anschließend die Bedingung  überprüft und falls diese korrekt ist, auch der zweite Aktor ausgeführt.
    \item \texttt{REPEAT}: Solange die Bedingung (hier wieder \glqq exp\grqq) erfüllt ist, wird der Aktor ausgeführt.
    \item \texttt{PARALLEL}: Beide Aktoren werden parallel ausgeführt.
    \item \texttt{PARALLEL_PORTFOLIO}: Eine beliebige Anzahl an Aktoren wird parallel ausgeführt, bis die Bedingung erfüllt ist. Das Ergebnis, welches als erstes die Bedingung erfüllt, wird als Resultat des \portfolio angesehen.
    
\end{itemize}
Ein Aktor lässt sich in zwei Kategorien aufteilen:
\begin{itemize}
    \item Komposition: Ein Aktor kann wieder eine der oben gelisteten Kompositionen sein.
    \item Externes Programm: Ein externes Programm (in \coveriteam \glqq atomic actor\grqq genannt), welches aus Sicht von \coveriteam unveränderbar ist.
    Dieses ist dann in den meisten Fällen ein Software-Verifizierungsprogramm wie zum Beispiel \cpachecker\cite{CPACHECKER}.
\end{itemize} 
Durch die mögliche Verschachtelung von Komposition ergibt sich damit ein Baum, dessen Blätter allerdings immer externe Programme sind. 
\cref{lst:portfolio-example-creation} zeigt den Code in der Sprache von \coveriteam um die Komposition aus \cref{alg:portfolio-example} zu erstellen.
\begin{minipage}{\linewidth}
    \begin{lstlisting}[language=CVT,tabsize=4, caption=Erstellung des Portfolios in der Sprache von \coveriteam,label=lst:portfolio-example-creation]
cpachecker = ActorFactory.create(
    ProgramVerifier, "../actors/cpa-seq.yml", "default"
);
esbmc = ActorFactory.create(
    ProgramVerifier, "../actors/esbmc-kind.yml", "default"
);

success_condition = ELEMENTOF(verdict, {TRUE,FALSE});

portfolio = PARALLEL_PORTFOLIO(
    cpachecker,
    esbmc, 
    success_condition
);
    \end{lstlisting}
\end{minipage}


\subsection{Ausführung}
Nachdem man ein \portfolio erzeugt hat, wird die Komposition ausgeführt.
Für diese Ausführung stellen wir zwei verschiedene Ausführungsmechanismen zur Verfügung.
Der erste Mechanismus nutzt \texttt{MPI} für Prozesskreation und Prozesskommunikation, der andere Prozesse von Pythons \texttt{multiprocessing} Modul.
Nach dem Start eines \portfolio wird dann für jeden Aktor ein eigener Prozess erstellt, welcher diesen dann ausführt. \ref{fig:process-spawning} zeigt die Prozesse, welche nach dem Start eines \portfolio aktiv sind.
\begin{figure}[h]
	\includegraphics[width=0.95\textwidth]{executions}
	\caption{Die erstellten Prozesse nach dem Starten des \portfolio}
	\label{fig:process-spawning}
\end{figure}