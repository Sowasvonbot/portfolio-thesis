import os
from subprocess import Popen 

script_dir = os.path.dirname(os.path.realpath(__file__))
procs = []

for file in filter(lambda file: ".tex" in file, os.listdir(script_dir)):
    path = "/".join(["plots",file])
    procs.append(Popen(f"pdflatex -output-directory=plots {path}", shell=True))
    
for process in procs:
	process.wait()

#print("\\input {plots/dummy.tex}")
