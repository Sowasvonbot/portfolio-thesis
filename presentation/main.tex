\documentclass[12pt,
	english,
	dvipsnames,
	aspectratio=169,
	%handout
]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{pdfpages}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{colortbl}
\usepackage{svg}
\usepackage{tabto}
\usepackage{sosy-beamer}
\input{../macros}
%\usetheme{metropolis}
%\usetheme{Frankfurt}
% \usecolortheme{owl}
\usepackage{sosy-common}

\usepackage{caption}

% \beamertemplatenavigationsymbolsempty
\author[Tobias Kleinert]{
  {\large\textbf{Tobias Kleinert}}\\
 Developing a Verifier Based on Parallel Portfolio with CoVeriTeam\\[2ex]
 \smaller Mentor: Sudeep Kanav\\
 Supervisor: Prof.~Dr.~Dirk Beyer}
\title{\LARGE{PARALLEL PORTFOLIO VERIFIER}}

\institute{LMU Munich, Germany}

\date{}

\titlegraphic{
  \vspace{-1cm}
  \hfill
  % Include pictures of authors here; emphasize presenting author by larger image
  %\rule{1.6cm}{2.0cm}%\includegraphics[height=2.0cm]{images/dirk-beyer}
  \hfill
  %\rule{1.7cm}{2.2cm}%\includegraphics[height=2.2cm]{images/matthias-dangl}
  \hfill
  %\rule{1.6cm}{2.0cm}%\includegraphics[height=2.0cm]{images/other-author}
  \hfill~\\
  \vspace{1.0cm}
  \resizebox{!}{1cm}{\input{logos/lmu}}\hfill
  \resizebox{!}{1cm}{\input{logos/cpa}}\hfill
  \resizebox{!}{1cm}{\input{logos/sosy}}
}

\definetool{\coveriteam}{CoVeriTeam}
\definetool{\mpi}{MPI}
\definetool{\portfolio}{Parallel Portfolio}
\definetool{\antlr}{ANTLR}
\definetool{\slurm}{SLURM}
\definetool{\openmpi}{OpenMPI}
\definetool{\mpiforpy}{mpi4py}
\definetool{\python}{Python}
\definetool{\mpich}{MPICH}
\definetool{\fortran}{Fortran}
% \definetool{\C}{C}

\floatname{algorithm}{Example}

% \setbeamercovered{dynamic} % Ueberblendung

\input{|python ../table-data/import-data-scripts.py}


% Experimental table only command:
\usepackage{array}
\newcolumntype\math[1]{>{\(}#1<{\)}}
\newcounter{iteration}
\makeatletter
\newcommand*\tableonly%>>>
  {%
    \omit\@ifnextchar<\table@only\table@@only
  }%<<<
\protected\long\def\table@only<#1>#2%>>>
  {%
    \gdef\beamer@doifnotinframe{\cr}%
    \def\beamer@doifinframe{\cr#2}%
    \beamer@masterdecode{#1}%
    \beamer@donow
  }%<<<
\protected\long\def\table@@only#1%>>>
  {%
    \beamer@ifnextcharospec{\table@@@only{#1}}{\cr#1}%
  }%<<<
\long\def\table@@@only#1<#2>%>>>
  {%
    \gdef\beamer@doifnotinframe{\cr}%
    \def\beamer@doifinframe{\cr#1}%
    \beamer@masterdecode{#2}%
    \beamer@donow
  }%<<<
\makeatother

%%%%% Decrease footnote size
%\let\oldfootnotesize\footnotesize
%\renewcommand*{\footnotesize}{\oldfootnotesize\tiny}


\begin{document}
\graphicspath{{../graphics/drawio/}{./plots/}{../}}
\svgpath{{../graphics/drawio/}}

%Titelseite
\frame{\titlepage}

\section{Motivation}

\begin{frame}{Motivation}
	\begin{itemize}
		\item Software Verification takes a lot of time
		\item Software Verification tools have different strengths
		\pause
		\item Example from \svcomp 22:
		\begin{itemize}
			\item \href{
				https://sv-comp.sosy-lab.org/2022/results/results-verified/META_ReachSafety.table.html\#/table?filter=id_any(value(while_infinite_loop_3))&hidden=0,1,2,3,5,6,8,9,10,11,12,13,14,15,16,17,18,19,20
			}{\texttt{loops/while_infinite_loop_3.c}}\\  
		\quad\cpachecker:\tabto{2.7cm} Solved in 5 s of CPU time\\
		\quad\esbmc:\tabto{2.7cm} timeout
		\pause
		\vspace{1ex}
			\item \href{
				https://sv-comp.sosy-lab.org/2022/results/results-verified/META_ReachSafety.table.html\#/table?filter=id_any(value(sanfoundry_10_ground))&hidden=0,1,2,3,5,6,8,9,10,11,12,13,14,15,16,17,18,19,20
			}{array-examples/sanfoundry_10_ground.c}\\
		\quad\cpachecker:\tabto{2.7cm} timeout\\
		\quad\esbmc:\tabto{2.7cm} Solved in less than 1 s of CPU time
		\end{itemize}
	\end{itemize}
	\pause
	\begin{center}
		\begin{minipage}{.6\textwidth}
			\begin{block}{}
				\begin{center}
					Combine tools and use their strengths
				\end{center}
			\end{block}
		\end{minipage}
	\end{center}
\end{frame}

\begin{frame}
\frametitle{Portfolio Verifier - Idea}
\pause
\begin{itemize}[<+->]
\item Take an arbitrary number of verifiers
\item Give them the same program and specification to verify
\item Run them in parallel
\item Take the first result which satisfies a given condition
\item Terminate the still running verifiers
\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Portfolio Verifier - Idea}
	\begin{center}
		\resizebox{.8\textwidth}{!}{
			\begin{minipage}{\textwidth}
				\begin{algorithm}[H]
					\caption*{\portfolio}
					\label{alg:portfolio-example}
					{\rmfamily
						\algorithmicrequire \quad Program p, Specification s\\
						\algorithmicensure \quad Verdict
						\begin{algorithmic}[1]
							\STATE verifier_1 $\coloneqq$ Verifier("CPAchecker")
							\STATE verifier_2 $\coloneqq$ Verifier("ESBMC")
							\STATE success_condition $\coloneqq$ verdict $\in \{\textit{TRUE},\,\textit{FALSE}\}$
							\STATE parallel_portfolio $\coloneqq$ ParallelPortfolio(\\\qquad verifier_1, \\\qquad verifier_2, \\\qquad success_condition\\)
							\STATE result $\coloneqq$ parallel_portfolio.verify(p,s)
							\RETURN (result.verdict,\,result.witness)
						\end{algorithmic}
					}
				\end{algorithm}
			\end{minipage}
		}
	\end{center}
\end{frame}

\section{CoVeriTeam}

\begin{frame}{\coveriteam}
	\begin{itemize}
		\item Builds compositions of existing verification tools
		\pause
		\item Main parts of \coveriteam
		\begin{itemize}
			\item \textbf{Artifact:} Files and results
			\item \textbf{Actor:} Uses artifacts as input and produces new ones
			\begin{itemize}
				\item \textbf{Composition} of multiple actors
				\item \textbf{Atomic actor:} External verification tools
				\item \textbf{Utility actor:} Manipulates artifacts
			\end{itemize}
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{\coveriteam}
	\begin{figure}
		\includegraphics[width=.6\textwidth]{coveriteam-abstract}
		\caption*{Execution in \coveriteam}
	\end{figure}
	\begin{itemize}[<+->]
		\item Uses own lightweight language to describe compositions (in text format)
		\item Converts these descriptions to \python code 
		\item Downloads (if necessary) and executes actors
		\item Returns the produced artifacts
	\end{itemize}
\end{frame}

\section{Implementation}

\begin{frame}{\portfolio implementation}
	\begin{figure}
		\includegraphics[width=.6\textwidth]{coveriteam-abstract}
		\caption*{Execution in \coveriteam}
	\end{figure}
	\begin{itemize}
		\item Extended the input language of \coveriteam
		\item Extended the code generation
		\pause
		\item Implemented a new composition \portfolio in \coveriteam
		\begin{itemize}
			\item Type check
			\item Execution
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Execution}
\begin{itemize}[<+->]
\item Parallel execution $\Rightarrow$ multiple processes
\item Each process executes one actor
\item Sends result back to main process
\item Main process evaluates \texttt{success_condition} with result
\item If success condition is true stop the still running processes otherwise wait
\item Spawning and communication of these processes with \mpi
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\mpi}
\begin{itemize}
\item Message-Passing-Interface (\mpi)
\item Used to exchange messages between processes
\item Exchange of messages in groups of processes, so called \texttt{communicators}
\item Different kinds of exchange of messages
\begin{itemize}
\item One-to-one messages (e.g. \texttt{MPI_Send, MPI_Isend})
\item Collective message operation (e.g. \texttt{MPI_Broadcast})
\end{itemize}
\item Available only in \texttt{C}/\texttt{C++} and \fortran (we used \mpiforpy)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Use of \mpi}
\begin{columns}
\column{.5\textwidth}
\begin{figure}
\includegraphics[width=.8\linewidth]{mpi-execution}
\end{figure}
\pause
\column{.5\textwidth}
\begin{itemize}
\item \texttt{MPI Scheduler}: Spawns worker and receives their results
\item \texttt{QueueManager}: Synchronize \coveriteam process and \texttt{MPI Scheduler}
\item \texttt{MPI Worker}: Executes actors
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Fallback execution}
\begin{columns}
\column{.5\textwidth}
\begin{itemize}
\item \mpi execution needs \mpi implementation and \mpiforpy installed\\
$\rightarrow$ fallback execution, if one not present
\pause
\item Uses only \python
\pause[3]
\item Similar, but less complicated setup
\item Sharing one \texttt{Queue} for result sending
\end{itemize}
\pause[2]
\column{.5\textwidth}
\begin{figure}
\includesvg[height=.7\textheight]{fallback-execution}
\end{figure}
\end{columns}
\end{frame}

\section{Evalutation}

\begin{frame}{Evaluation}
	\begin{columns}[t]
		\column{.5\textwidth}
		\textbf{Experiments:}
		\begin{itemize}
			\item Fallback vs \mpi Execution
			\item \portfolio with Verifier + Validator combination
			\item Execution on a cluster
		\end{itemize}
		\pause
		\column{.5\textwidth}
		\textbf{Tool selection:}
		\begin{enumerate}[<+->]
			\item \cpachecker
			\item \esbmc
			\item \symbiotic
		\end{enumerate}
		\pause
		\vspace{2ex}
		\scriptsize Same selection as Beyer, Kanav, and Richter in "\textit{Construction of Verifier
			Combinations Based on Off-the-Shelf Verifiers}"
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Evaluation}
	\begin{columns}
		\column{.5\textwidth}
		\begin{itemize}
			\item Evaluation with \benchexec
			\item Benchmark-set for the \texttt{unreach call} specification\\
			Contains $8883$ tasks
			\item Comparison with \cpachecker
			\item Tools from \svcomp 21
		\end{itemize}
		\column{.5\textwidth}
		\pause
		\textbf{Resources:}
		\begin{itemize}
			\item CPU: Intel Xeon E3-1230 v5 @ 3.40 GHz (apollon*) (except cluster)
			\item RAM: 15 GB (in most runs)
			\item CPU time: 15 min (in most runs)
			\item \mpi Implementation: \openmpi v4.0.3 (except cluster)
		\end{itemize}
	\end{columns}
\end{frame}


\begin{frame}[t]
\frametitle{Evaluation - \mpi vs. Fallback}
\begin{center}
\resizebox{.55\textwidth}{!}{
\setlength{\tabcolsep}{6pt}
\begin{tabular}{l@{\hskip 10pt}c@{\hskip 12pt}ccc}
\toprule
\multicolumn{1}{l}{\multirow{2}{*}{}} & \cpachecker & \portfolio & \portfolio\\ & & \mpi &   fallback \\ 
\midrule
\tableonly<1->{
Score & 
\CpaSeqScore & 
\ThreeCompleteScore & 
\ThreeFallbackCompleteScore \\}
%%%%%% Correct
\tableonly<2->{
\midrule
Correct & 
\CpaSeqSvCompUnreachCallCorrectCount & 
\ThreeCompleteSvCompUnreachCallCorrectCount & 
\ThreeFallbackCompleteSvCompUnreachCallCorrectCount \\
\quad True &
\CpaSeqSvCompUnreachCallCorrectTrueCount & 
\ThreeCompleteSvCompUnreachCallCorrectTrueCount & 
\ThreeFallbackCompleteSvCompUnreachCallCorrectTrueCount \\ 
\quad False &
\CpaSeqSvCompUnreachCallCorrectFalseCount & 
\ThreeCompleteSvCompUnreachCallCorrectFalseUnreachCallCount & 
\ThreeFallbackCompleteSvCompUnreachCallCorrectFalseUnreachCallCount \\ }
%%%%%% Wrong
\tableonly<3->{
\midrule
Wrong & 
\CpaSeqSvCompUnreachCallWrongCount & 
\ThreeCompleteSvCompUnreachCallWrongCount & 
\ThreeFallbackCompleteSvCompUnreachCallWrongCount \\ 
\quad True &
0 & 
\ThreeCompleteSvCompUnreachCallWrongTrueCount & 
\ThreeFallbackCompleteSvCompUnreachCallWrongTrueCount \\ 
\quad False &
\CpaSeqSvCompUnreachCallWrongFalseCount & 
\ThreeCompleteSvCompUnreachCallWrongFalseUnreachCallCount & 
\ThreeFallbackCompleteSvCompUnreachCallWrongFalseUnreachCallCount \\ }
\tableonly<4->{
\midrule
\multicolumn{4}{l}{Resource Consumption} \\
\quad CPU time (h)& 
\rndnum{\stoh{\CpaSeqSvCompUnreachCallTotalCputime}} & 
\rndnum{\stoh{\ThreeCompleteSvCompUnreachCallTotalCputime}} & 
\rndnum{\stoh{\ThreeFallbackCompleteSvCompUnreachCallTotalCputime}} \\
\quad Wall-time (h)& 
\rndnum{\stoh{\CpaSeqSvCompUnreachCallTotalWalltime}} & 
\rndnum{\stoh{\ThreeCompleteSvCompUnreachCallTotalWalltime}} & 
\rndnum{\stoh{\ThreeFallbackCompleteSvCompUnreachCallTotalWalltime}} \\
\quad Energy (KJ)& 
\rndnum{\toKilo{\CpaSeqSvCompUnreachCallTotalEnergy}} & 
\rndnum{\toKilo{\ThreeCompleteSvCompUnreachCallTotalEnergy}} & 
\rndnum{\toKilo{\ThreeFallbackCompleteSvCompUnreachCallTotalEnergy}} \\
}
\bottomrule
\end{tabular}
}
\end{center} 
\begin{block}{Results}<5->
\begin{itemize}
	\small
	\item More solved tasks, but also more wrong results\\$ \rightarrow $ Score of \cpachecker and \portfolio (\mpi) about the same
	\item Fallback \portfolio performed better than \mpi\relax \portfolio
\end{itemize}
\end{block}
\end{frame}


\begin{frame}
\frametitle{Evaluation - \mpi vs. Fallback - CPU time}
\begin{columns}
\column{.4\textwidth}<1->
\begin{figure}
\includegraphics[width=\textwidth]{plots/pres-scatter-3-vs-fallback-cputime-log}
\caption*{Log comparison}
\end{figure}
\column{.4\textwidth}<2->
\begin{figure}
\includegraphics[width=\textwidth]{plots/pres-scatter-3-vs-fallback-cputime-linear}
\caption*{Linear comparison}
\end{figure}
\end{columns}
\begin{block}{Results}<3->
	\begin{itemize}
		\small
		\item Linear increase of CPU time difference due to busy waiting in \texttt{MPI Scheduler}
	\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Evaluation - \mpi vs. Fallback - Wall time}
\begin{figure}
\includegraphics[width=.4\textwidth]{plots/pres-base-comparison-walltime}
\end{figure}
\pause
\begin{block}{Results}
	\begin{itemize}
		\small
		\item Lower walltime for fast tasks in \texttt{Fallback}\\
		$ \Rightarrow $ Setup for python processes needs less time
	\end{itemize}
\end{block}
\end{frame}

\begin{frame}
	\frametitle{Evaluation - Validating portfolio}
	\begin{center}
		\resizebox{!}{.4\textheight}{
			\begin{minipage}{\textwidth}
				\begin{algorithm}[H]
					\caption*{Validating \portfolio}
					{\rmfamily
						\algorithmicrequire \quad Program p, Specification s\\
						\algorithmicensure \quad Verdict
						\begin{algorithmic}[1]
							\STATE verifier_1 $\coloneqq$ Verifier("CPAchecker")\\ \dots
							\STATE validator $\coloneqq$ Validator("CPAchecker-Validator")
							\STATE verifier_1 $\coloneqq$ SEQUENCE(verifier_1, validator)\\
							\dots
							\STATE success_condition $\coloneqq$ (verdict == verdict_validator) $\in \{TRUE\}$
							\STATE parallel_portfolio $\coloneqq$ ParallelPortfolio(\\\qquad verifier_1, \\\qquad verifier_2, \\\qquad verifier_3,\\\qquad success_condition\\)
							\STATE result $\coloneqq$ parallel_portfolio.verify(p,s)
							\RETURN (result.verdict,\,result.witness)
						\end{algorithmic}
					}
				\end{algorithm}
			\end{minipage}
		}
	\end{center}
\end{frame}

\begin{frame}
\frametitle{Evaluation - Validating portfolio}
\begin{center}
\resizebox{.5\textwidth}{!}{
\setlength{\tabcolsep}{6pt}
\begin{tabular}{l@{\hskip 10pt}c@{\hskip 12pt}ccc}
\toprule
\multicolumn{1}{l}{\multirow{2}{*}{}} & \portfolio & \portfolio\\ & \mpi &  validating \\ 
\toprule
\tableonly<1->{
Score & 
\ThreeCompleteScore & 
\ThreeValidatingCompleteScore \\}
%%%%%% Correct
\tableonly<2->{
\midrule
Correct & 
\ThreeCompleteSvCompUnreachCallCorrectCount & 
\ThreeValidatingCompleteSvCompUnreachCallCorrectCount \\
\quad True &
\ThreeCompleteSvCompUnreachCallCorrectTrueCount & 
\ThreeValidatingCompleteSvCompUnreachCallCorrectTrueCount \\ 
\quad False &
\ThreeCompleteSvCompUnreachCallCorrectFalseUnreachCallCount & 
\ThreeValidatingCompleteSvCompUnreachCallCorrectFalseCount \\ }
%%%%%% Wrong
\tableonly<3->{
\midrule
Wrong & 
\ThreeCompleteSvCompUnreachCallWrongCount & 
\ThreeValidatingCompleteSvCompUnreachCallWrongCount \\ 
\quad True &
\ThreeCompleteSvCompUnreachCallWrongTrueCount & 
\ThreeValidatingCompleteSvCompUnreachCallWrongTrueCount \\ 
\quad False &
\ThreeCompleteSvCompUnreachCallWrongFalseUnreachCallCount & 
\ThreeValidatingCompleteSvCompUnreachCallWrongFalseCount \\ }
\bottomrule
\end{tabular}
}
\end{center} 
\begin{block}{Results}<4->
	\begin{itemize}
		\small
		\item Minimized the wrong results
		\item Total number of results reduced drastically
	\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Evaluation - Validating portfolio - Tasks False}
\begin{figure}
\includegraphics[width=.9\textwidth]{plots/pres-scatter-validating-cputime-false}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Evaluation - Validating portfolio - Tasks True}
\begin{figure}
\includegraphics[width=.9\textwidth]{plots/pres-scatter-validating-cputime-true}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Evaluation - Cluster}
\begin{columns}[t]
\column{.5\textwidth}
\textbf{Special Test Setup:}
\begin{itemize}[<+->]
	\item Composition of 12 Verifiers (all from \svcomp 21 except \veriabs)
	\item Executed on a cluster of 4 machines
	\item \benchexec as benchmark framework (no CPU time and energy measurement)
\end{itemize}
\pause[4]
\column{.5\textwidth}
\textbf{Resources:}
\begin{itemize}
	\item CPU: Intel Core i7-6700 @ 3.40 GHz (ws*)
	\item RAM: 30 GB
	\item CPU time: 30 min
	\item \mpi Implementation: \mpich
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}{Evaluation - Cluster}
	\begin{center}
		\resizebox{.5\textwidth}{!}{
			\setlength{\tabcolsep}{6pt}
			\begin{tabular}{l@{\hskip 10pt}c@{\hskip 12pt}ccc}
				\toprule
				\multicolumn{1}{l}{\multirow{2}{*}{}} & \portfolio & \portfolio\\ & \mpi &  cluster \\ 
				\toprule
				\tableonly<1->{
					Score & 
					\ThreeCompleteScore & 
					\ClusterScore \\}
				%%%%%% Correct
				\tableonly<2->{
					\midrule
					Correct & 
					\ThreeCompleteSvCompUnreachCallCorrectCount & 
					\ClusterCorrectCount \\
					\quad True &
					\ThreeCompleteSvCompUnreachCallCorrectTrueCount & 
					\ClusterCorrectTrueCount \\ 
					\quad False &
					\ThreeCompleteSvCompUnreachCallCorrectFalseUnreachCallCount & 
					\ClusterCorrectFalseCount \\ }
				%%%%%% Wrong
				\tableonly<3->{
					\midrule
					Wrong & 
					\ThreeCompleteSvCompUnreachCallWrongCount & 
					\ClusterWrongCount \\ 
					\quad True &
					\ThreeCompleteSvCompUnreachCallWrongTrueCount & 
					\ClusterWrongTrueCount \\ 
					\quad False &
					\ThreeCompleteSvCompUnreachCallWrongFalseUnreachCallCount & 
					\ClusterWrongFalseCount \\ }
				\tableonly<4->{
					\midrule
					\multicolumn{3}{l}{Resource Consumption} \\
					\quad Wall-time (h)& 
					\rndnum{\stoh{\ThreeCompleteSvCompUnreachCallTotalWalltime}} & 
					\rndnum{\stoh{\ClusterTotalWalltime}} \\}
				\bottomrule
			\end{tabular}
		}
	\end{center}
\begin{block}<5->{Results}
	\begin{itemize}
		\small
		\item A lot of solved tasks
		\item Also a lot of incorrect results
		\item Only slight increase of the score for the available resources\\(Used 4 machines instead of one)
	\end{itemize}
\end{block}
\end{frame}

\section{Conclusion}

\begin{frame}{Conclusion}
	\begin{itemize}[<+->]
		\item Implemented \portfolio composition in \coveriteam
		\item Fast (in terms of wall-time) and energy efficient execution
		\item Relatively high amount of wrong results (due to the nature of the \portfolio)
		\item Fast fallback execution
		\item Cluster capability
	\end{itemize}
\end{frame}

\begin{frame}{Conclusion}
	\textbf{Special achievements:}
	\begin{itemize}
		\item The \portfolio participated in the \svcomp 22 (not as competitor)
		\begin{itemize}
			\item Second place in \texttt{ReachSafety}
			\item Even first place in \texttt{NoOverflows}
		\end{itemize}
		\pause
		\item The \portfolio was used in the paper "\textit{Construction of Verifier
		Combinations Based on Off-the-Shelf Verifiers}" from Beyer, Kanav, and
		Richter.
	\end{itemize}
\end{frame}

\begin{frame}{Future work}
	\begin{itemize}
		\item Investigate the validating \portfolio
		\begin{itemize}
			\item Run on a cluster
			\item Use of different validators
		\end{itemize}
		\pause
		\item \portfolio of testers
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{center}
		\LARGE Thank you!
	\end{center}
\end{frame}

\section{Appendix}

\appendix



\begin{frame}
	\frametitle{\mpi Environment}
	\begin{itemize}
		\item \mpi programs need to be started with \texttt{mpiexec}
		\item Spawning new process with \texttt{mpiexec}
		\item Process executes \python interpreter \\
		Command: "\texttt{mpiexec -n 1 python mpi-scheduler.py}"
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Process spawning}
	\begin{columns}[t]
		%
		\column{.5\textwidth}
		\textbf{Static}\\[1ex]
		%
		\begin{itemize}
			\item Need to calculate the needed processes
			\item Grouping processes difficult
			\item Easy to use with \slurm
		\end{itemize}
		%
		\column{.5\textwidth}
		\textbf{Dynamic}\\[1ex]
		%
		\begin{itemize}
			\item Processes are spawned when needed \vspace{\baselineskip}
			\item Processes are grouped by default
			\item \texttt{MPI_Comm_Spawn} not supported with \openmpi in combination with \slurm
		\end{itemize}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Busy waiting}
	\begin{columns}
		\column{.5\textwidth}
		\begin{itemize}
			\item \mpi uses busy waiting as waiting strategy \\
			$\Rightarrow$ A lot of CPU time is wasted
			\item Solution: Manual pause of the check for new messages in \texttt{MPI Worker}
			\item \texttt{MPI Scheduler} still uses busy waiting 
		\end{itemize}
		\column{.5\textwidth}
		\begin{figure}
			\includegraphics[width=.8\linewidth]{mpi-execution}
		\end{figure}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Shutdown of actors}
	\begin{itemize}
		\item New Feature in \coveriteam : Shutdown of actors
		\item Shutdown procedure for each composition
		\begin{itemize}
			\item Parallel $\rightarrow$ Stop both actors
			\item Sequence $\rightarrow$ Stop first, then second actor
			\item ITE $\rightarrow$ Invalidate condition, then stop first actor
			\item Repeat $\rightarrow$ Invalidate repeat condition, then stop actor
		\end{itemize}
		\item Shutdown for external tools (atomic actors)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Stopping of atomic actors}
	\begin{columns}
		\column{.5\textwidth}
		\begin{figure}
			\includesvg[width=\textwidth]{shutdown}
		\end{figure}
		\pause
		\column{.5\textwidth}
		\begin{itemize}
			\item First, only boolean flag
			\item Bug occurred with some tools
			\item $\Rightarrow$ more complex shutdown procedure
		\end{itemize}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Exiting the \mpi environment}
	\begin{itemize}
		\item Result are sent back with the \texttt{QueueManager}
		\item \texttt{MPI Scheduler} shuts down every \texttt{MPI Worker}
		\item \texttt{MPI Scheduler} catches signals to terminate the workers 
	\end{itemize}
\end{frame}

\end{document}
