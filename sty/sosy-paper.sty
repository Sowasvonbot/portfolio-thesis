\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesPackage{sosy-paper}[2018/02/01 Package for papers of SoSy-Lab]

\RequirePackage{etoolbox} % low-level commands for tricky macro stuff
\RequirePackage{kvoptions} % for advanced options handling
\SetupKeyvalOptions{family=sosypaper, prefix=sosypaper@}

% store actual title separately (needed for web option and for hyperref metadata)
\AtEndPreamble{\let\our@title\@title}

% Option "web" includes text set with \copyrightheader reference on top of paper
\DeclareVoidOption{web}{
  \AtEndPreamble{
    \title{
      \color{white}.\color{black}\\
      \vspace{-28mm}
      {\color{gray}\small\sffamily\bfseries \@copyrightheader}\\
      \vspace{16mm}
      \our@title
    }
    \renewcommand\title[1]{\errmessage{Do not use command title in body, move it to preamble, please}}
  }
}

\DeclareBoolOption[true]{sloppy} % option for enabling \sloppy, default is true

\ProcessKeyvalOptions*{}

\ifsosypaper@sloppy
  \sloppy
\fi

\newbool{springer}
\newbool{springer-journal}
\@ifclassloaded{svjour}{\booltrue{springer}\booltrue{springer-journal}}{}
\@ifclassloaded{svjour3}{\booltrue{springer}\booltrue{springer-journal}}{}
\@ifclassloaded{llncs}{\booltrue{springer}}{}

% Commands for defining content of copyright header
\newcommand\@copyrightheader{\errmessage{Please use command copyrightheader to define copyright notice}}
\newcommand\copyrightheader[1]{\gdef\@copyrightheader{#1}}


% encoding and fonts first
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{microtype}
% \pdfcompresslevel=9

% If we want to use Times (ugly, but if we really need more space)
%\RequirePackage{mathptmx}

% General packages
\RequirePackage{calc}
\RequirePackage{expl3}
\RequirePackage{fixltx2e}
\RequirePackage{xspace}
\RequirePackage{relsize} % for \smaller
% Declare command \fpeval{} for floating-point computations.
\ExplSyntaxOn
  \cs_new_eq:NN \fpeval \fp_eval:n
\ExplSyntaxOff


%%%%%%%%%%%%% BIBLIOGRAPHY %%%%%%%%%
\bibliographystyle{abbrv}
% Our custom abbrv.bst with link boxes uses minipages,
% which breaks the spacing (at least with svjour and llncs), thus add some space:
\AtBeginEnvironment{thebibliography}{
\BeforeBeginEnvironment{minipage}{\vspace{.3\baselineskip}}
}{}
% To have the citation lists ordered by number.
\RequirePackage[nocompress]{cite}
% Combine [1], [3] into [1, 3] (from https://tex.stackexchange.com/questions/43993/ieee-latex-style-grouping-references)
\renewcommand{\citepunct}{,$\citepunctpenalty$\,}


%%%%%%%%%%%%% MATH %%%%%%%%%%
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{stackrel}
\ifbool{springer}{
  % conflict
}{
  \RequirePackage{amsthm}
}


%%%%%%%%%%%%% TEXT %%%%%%%%%%
\RequirePackage{flushend} % automatically balances last page
\RequirePackage[defblank]{paralist}
\RequirePackage[lowtilde]{url}
\RequirePackage{enumitem}
\RequirePackage[printonlyused,nolist]{acronym}
\RequirePackage{cancel}
\RequirePackage{engord} % for simple ordinal numbers with \engordnumber{5}
\RequirePackage[group-digits=integer, group-minimum-digits=4,
                list-final-separator={, and }, add-integer-zero=false,
                free-standing-units, unit-optional-argument, % easier input of numbers with units
                binary-units,
                detect-weight=true,detect-inline-weight=math, % for bold cells
                per-mode=fraction, % we use fractions for units instead of negative exponents
                ]{siunitx}
\newcommand\calcPercent[2][]{\SI[round-mode=figures, round-precision=2, #1]{\fpeval{(#2) * 100}}{\percent}}
\robustify{\bfseries} % for \bfseries in table cells (http://tex.stackexchange.com/a/66256)
%\renewcommand{\labelitemi}{$\bullet$} % bullets as list items

\hyphenation{CEGAR Blast Tvla CPAchecker Impact}

\frenchspacing % No wide space between sentences

% Deny widows and orphans.
\clubpenalty = 10000
\widowpenalty = 10000
\displaywidowpenalty = 10000

% Footnotes without marker symbol
\def\blfootnote{\gdef\@thefnmark{}\@footnotetext}

% Define our own font size between footnotesize and scriptsize
\newcommand*\halfsmall{\@setfontsize\halfsmall{7.8}{9.4}}

%% Heading formatting for LNCS format
% Provide lengths that influence the vertical space around headings in LNCS format.
% The default is 1pt and it can be scaled down to remove vertical space.
\newlength{\sectionspace}      \setlength{\sectionspace}{\p@}
\newlength{\subsectionspace}   \setlength{\subsectionspace}{\sectionspace}
\newlength{\subsubsectionspace}\setlength{\subsubsectionspace}{\subsectionspace}
\newlength{\paragraphspace}    \setlength{\paragraphspace}{\subsubsectionspace}
\@ifclassloaded{llncs}{
% Definitions for being able to add a period after \subsubsection.
% Also define \nopunct to be able to remove it on case-by-case basis.
% Example: "\subsubection{Foo\nopunct} does bar."
\newcommand{\@period}{.}
\newcommand{\addperiod}[1]{#1\@period{} }
\newcommand{\nopunct}{\let\@period\relax}
% The following is a copy of the relevant part of llncs.cls,
% only with \p@ replaced by \sectionspace, \subsectionspace etc.
% Prefer to do not change this directly to stay consistent with LNCS.
\renewcommand\section{\@startsection{section}{1}{\z@}%
                       {-18\sectionspace \@plus -4\sectionspace \@minus -4\sectionspace}%
                       {12\sectionspace \@plus 4\sectionspace \@minus 4\sectionspace}%
                       {\normalfont\large\bfseries\boldmath
                        \rightskip=\z@ \@plus 8em\pretolerance=10000 }}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                       {-18\subsectionspace \@plus -4\subsectionspace \@minus -4\subsectionspace}%
                       {8\subsectionspace \@plus 4\subsectionspace \@minus 4\subsectionspace}%
                       {\normalfont\normalsize\bfseries\boldmath
                        \rightskip=\z@ \@plus 8em\pretolerance=10000 }}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                       {-18\subsubsectionspace \@plus -4\subsubsectionspace \@minus -4\subsubsectionspace}%
                       {-0.5em \@plus -0.22em \@minus -0.1em}%
                       {\normalfont\normalsize\bfseries\boldmath}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                       {-12\paragraphspace \@plus -4\paragraphspace \@minus -4\paragraphspace}%
                       {-0.5em \@plus -0.22em \@minus -0.1em}%
                       {\normalfont\normalsize\itshape}}
% The following redefines \subsubsection and \paragraph to add a period
% instead of hspace.
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                       {-18\subsubsectionspace \@plus -4\subsubsectionspace \@minus -4\subsubsectionspace}%
                       {\z@}%
                       {\normalfont\normalsize\bfseries\boldmath\addperiod}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                       {-12\paragraphspace \@plus -4\paragraphspace \@minus -4\paragraphspace}%
                       {\z@}%
                       {\normalfont\normalsize\itshape\addperiod}}
}{
  \ifbool{springer-journal}{\newcommand\nopunct{}}{}
}

% Definition of special small paragraph or subsection headings
% that look always the same, independently from the definitions in the main LaTeX class
\newcommand{\smallsec}[1]{\medskip\noindent{\bfseries #1.}}
\newcommand{\smallersec}[1]{\medskip\noindent{\it #1.}}

%%%%%%%%%%%%% TABLES %%%%%%%%%%
\RequirePackage{longtable}
\RequirePackage{afterpage} % for placing longtables like floats
\RequirePackage{booktabs}
\RequirePackage{multirow}
\RequirePackage{rotating} % for rotated cells
% \RequirePackage{slashbox} % for table cell with diagonal line (needs extra file)

% For tables with automatic width of columns
% even in "p" columns.
\RequirePackage{tabulary}
\let\@oldfootnote\footnote
\AtBeginEnvironment{tabulary}{%
  % Footnotes in tabulary need special casing, otherwise they occur twice.
  \renewcommand{\footnote}[1]{\ifx\[$\else\@oldfootnote{#1}\fi}
}
\AtBeginEnvironment{table}{%
  \renewcommand\footnoterule{}% no rule below table, table should have \bottomrule
}

% table columns aligned on decimal point (superseded by siunitx)
%\RequirePackage{dcolumn}
%\newcolumntype{d}{D{.}{.}{2.1}}
%\newcolumntype{e}{D{.}{.}{3.1}}
%\newcolumntype{f}{D{.}{.}{4.1}}
%\newcolumntype{g}{D{.}{.}{5.1}}
%\newcolumntype{h}{D{.}{.}{2.2}}
%\newcolumntype{i}{D{.}{.}{3.2}}
%\newcolumntype{j}{D{.}{.}{4.2}}
%\newcolumntype{k}{D{.}{.}{5.2}}
%\newcolumntype{m}{D{.}{.}{1.2}}

% Provide a command \dcolcolor that allows chaning the color of a dcolumn cell
% https://tex.stackexchange.com/a/42768/16684
%\def\DC@endright{$\hfil\egroup\@dcolcolor\box\z@\box\tw@\dcolreset}
%\def\dcolcolor#1{\gdef\@dcolcolor{\color{#1}}}
%\def\dcolreset{\dcolcolor{black}}
%\dcolcolor{black}


%%%%%%%%%%%%% FIGURES %%%%%%%%%%
% GnuPlot generates PDF 1.5, whereas LaTeX by default generates PDF 1.4
% and produces a warning about the figures.
% This increments the version of the resulting PDF to 1.5
% such that included figures in PDF 1.5 are legal.
% \pdfminorversion=5

\RequirePackage[]{caption} % justification=centering for centered captions

% Colors
\RequirePackage[table]{xcolor}
\newcommand\definergbcolor[2]{\definecolor{#1}{rgb}{#2}}
\newcommand\definecmykcolor[2]{\definecolor{#1}{cmyk}{#2}}
\definergbcolor{cpacheckergreen}{0 0.63529414 0}
\definergbcolor{sosyblue}{0.34117648 0.74117649 1}
%\definergbcolor{unigrey}{1 0.50196081 0}
%\definergbcolor{uniorange}{0.48627451 0.52941179 0.49411765}
\definecmykcolor{uniorange}{0 0.5 1 0}
\definecmykcolor{unigrey}{0.08 0 0.06 0.47}

\RequirePackage{graphicx}
\RequirePackage{wrapfig}
% Set the root path for the graphics and figures.
\graphicspath{{figures/}{images/}}

\newcommand{\figurerule}{\hrule width \hsize height .33pt} %%% rule definition copied from sigplanconf.cls

% Allow more than 70% (default) of a page to be filled by figures (100%).
\renewcommand\topfraction{1.0}
\renewcommand\dbltopfraction{1.0}
\renewcommand\textfraction{0.0}

% Enable this to use the space below semi-large floats (50% - 80% of page) for text
\renewcommand\floatpagefraction{1.0}
\renewcommand\dblfloatpagefraction{1.0} % 0.85 to have text only below the smaller explicit-value analysis table

%\RequirePackage{floatflt} % Text around floats

\RequirePackage[labelfont={rm,bf,up}, farskip=0pt,]{subfig} % Intended to replace subfigure
%\RequirePackage[tight,TABTOPCAP]{subfigure}
%\renewcommand\thesubtable{\,\alph{subtable})}
%\RequirePackage{subcaption}

%\RequirePackage[all]{xy}

\RequirePackage{pgfplots} % for plots together with tikz
\RequirePackage{pgfplotstable}
\pgfplotsset{compat=1.13,
             log ticks with fixed point, % no scientific notation in logarithmic plots
             scaled ticks=false, % no scientific notation in plots
             table/col sep=tab, % only tabs are column separators
             unbounded coords=jump, % better have skips in a plot than appear to be interpolating
             filter discard warning=false, % Don't complain about empty cells
             }
\SendSettingsToPgf % use siunitx formatting settings in PGF, too
% Define new pgfmath function for the logarithm to base 10 that also works with fpu library
\pgfmathdeclarefunction{lg10}{1}{\pgfmathparse{ln(#1)/ln(10)}}

% Allows inclusion of figures from separate files
% that can also be compiled on their own:
\RequirePackage{standalone}

\RequirePackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{automata}
\usetikzlibrary{calc}
\usetikzlibrary{chains}
\usetikzlibrary{fadings}
\usetikzlibrary{fit}
\usetikzlibrary{patterns}
\usetikzlibrary{positioning}
\usetikzlibrary{shadows}
\usetikzlibrary{shapes}
\usetikzlibrary{shapes.geometric}
% Marker for tikz arrows in text:
\newcommand\tikzmark[1]{\tikz[remember picture,overlay]\node[inner ysep=0pt] (#1) {};}
% Gears in tikz:
\newcommand{\gear}[5]{%
\foreach \i in {1,...,#1} {%
  [rotate=(\i-1)*360/#1]  (0:#2)  arc (0:#4:#2) {[rounded corners=1.5pt]
             -- (#4+#5:#3)  arc (#4+#5:360/#1-#5:#3)} --  (360/#1:#2)
}}
% Checkmark
\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

% for trees with tikz
\usepackage{tikz-qtree}

% for graphs with tikz
\usepackage{sty/tkz-graph}

\RequirePackage[noend]{algorithmic}
\RequirePackage{algorithm}
%\RequirePackage[section]{algorithm}
% Fix line counters if multiple algorithms exist
\@addtoreset{ALC@line}{algorithm}
\@addtoreset{ALC@unique}{algorithm}

\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\renewcommand{\algorithmiccomment}[1]{\quad// #1}
\newcommand{\VARDECL}{\item[\textbf{Variables:}]}

\RequirePackage{fancyvrb}
\RequirePackage{listings}

% load a font that supports bold monospace (Courier)
\newcommand*{\ttfamilywithbold}{\fontfamily{pcr}\selectfont}
\newcommand*{\lstbasicfontsize}{\footnotesize} % Allow overriding our default font size of listings
\lstset{
    basicstyle=\ttfamilywithbold\lstbasicfontsize,
%    frame=single,
    numberblanklines=true,
    columns=fixed,
%    aboveskip=2pt,
%    belowskip=1pt,
    lineskip=-0.5pt,
    numbers=left,
    numberstyle=\tiny,
%    stepnumber=5,
    numberfirstline=true,
    firstnumber=1,
    xleftmargin=15pt,
    captionpos=b,
    numberbychapter=false,
}

\lstdefinestyle{C}{
    language=C,
    morekeywords={assert},
}
\lstdefinestyle{XML}{
    language=XML,
    showstringspaces=false,
    morecomment=[s]{!--}{--},
    markfirstintag=true,
    %keywordstyle=\bfseries,                          % XML tag names
    %tagstyle=,                                       % names of attributes and </>
    %stringstyle=,                                    % values of attributes
    commentstyle=\normalfont\itshape\color{black!80}, % comments
    basicstyle=\lstbasicfontsize\ttfamilywithbold,    % everything else (tag content)
}
\lstdefinelanguage{SMTLIB2}{
  classoffset=0,
  morekeywords={assert,check-sat,declare-fun,define-fun,get-model,let,set-option},
  alsoletter={-},
  morecomment=[l];,
  sensitive=true,
  }

% For syntax diagrams / BNF notation
\RequirePackage[rounded]{syntax}
\let\oldsdlengths\sdlengths
\renewcommand\sdlengths{\oldsdlengths\setlength{\sdfinalskip}{0pt}}

% Nice frame boxes with full page width
\RequirePackage[framemethod=tikz]{mdframed}
\mdfdefinestyle{nomargins}{
  innerleftmargin=0pt, innerrightmargin=0pt, innertopmargin=0pt, innerbottommargin=0pt,
  leftmargin=0pt, rightmargin=0pt, skipabove=0pt, skipbelow=0pt,
}
\tikzset{
  trailing dashes/.style={
    dash pattern=on 0pt off 1pt on 2pt off 2pt on 2pt off 3pt on 2pt off 10pt,
  },
  dense trailing dashes/.style={
    dash pattern=on 0pt off 1pt on 1pt off 2pt on 1pt off 2pt on 1pt off 10pt,
  },
  example page break/.style={
    trailing dashes, % or dense trailing dashes
    color=white,
    line width=6pt,
    overlay,
  },
}
\mdfdefinestyle{example}{
  % needspace=15\baselineskip, % alternative to nobreak for allowing breaks in large examples
  % next line produces thick gray bar on left instead of thin box
  linewidth=5pt, linecolor=black!20!white, topline=false, bottomline=false, rightline=false, innerrightmargin=0pt,
  firstextra ={\draw[example page break] let \p1=(P), \p2=(O) in (O) -- +(0,15pt);},
  secondextra={\draw[example page break] let \p1=(P), \p2=(O) in (\x2,\y1) -- +(0,-15pt);},
}

% Example environment with box
\ifbool{springer}{
  % environment example already defined
  \surroundwithmdframed[style=example]{example}
}{
  \theoremstyle{definition}
  \newmdtheoremenv[style=example, innertopmargin=0pt]{example}{Example}
}


%%%%%%%%%%%%% HYPERREF %%%%%%%%%%
% varioref for fancy page refs, needs to come before hyperref
\RequirePackage{varioref}
\@ifclassloaded{svjour}{
  % svjour is double-sided, but references like "on the facing page" do not make sense for papers
  \renewcommand\reftextfacebefore\reftextbefore
  \renewcommand\reftextfaceafter\reftextafter
}

% Hyperref should come after all other packages to fix compatibility problems.
\RequirePackage[%
  pdfpagemode=UseNone,% No sidebar in pdf viewer
  pdfpagelabels=false,plainpages=true,% Work if the document has no page numbers
  bookmarks=true,
  bookmarksnumbered=true,
  ]{hyperref}
\@ifclassloaded{llncs}{
  % No bookmarks, else generates wrong table of contents in PDF with LLNCS style
  \hypersetup{bookmarks=false}
}{}
\AtBeginDocument{
  \hypersetup{
    pdfauthor={\@ifclassloaded{svjour3}{\the\authorrunning}{\@author}},
    pdftitle={\our@title}
  }
}

% For disabling all colored link boxes
%\hypersetup{hidelinks}

% Cleveref helps in some cases like referencing the same footnote twice
% and needs to come after hyperref.
% Use \cref instead of \ref and \Cref at beginning of sentence to get automatic names for references.
\ifbool{springer-journal}{
  \let\cl@chapter\relax % bugfix for svjour(3) breaking cleveref
}{}
\RequirePackage[capitalise,nosort]{cleveref}
\newcommand{\creflastconjunction}{, and~}
\crefformat{footnote}{#2\footnotemark[#1]#3}
\crefname{section}{Sect.}{Sects.}
% line numbers in algorithms
\crefname{line}{line}{lines}
\crefalias{ALC@unique}{line}
\crefalias{ALC@line}{line}


%%%%%%%%%%%%% COMMENTS and MARKS %%%%%%%%%%
\RequirePackage{comment}
\newcommand\todo[1]{\textcolor{red}{\bfseries #1}}

\newcommand{\mynote}[1]{\textsf{$\clubsuit$ #1 $\clubsuit$ }}


%%%%%%%%%%%%% SPACE SQUEEZING %%%%%%%%%%
%% Reduce the space after algorithms
%\renewcommand\fs@ruled{\def\@fs@cfont{\bfseries}\let\@fs@capt\floatc@ruled
%  \def\@fs@pre{\kern4pt\hrule height.8pt depth0pt \kern2pt}%
%  \def\@fs@post{\kern0pt\hrule\relax\vspace{-5mm}}%
%  \def\@fs@mid{\kern2pt\hrule\kern2pt}%
%  \let\@fs@iftopcapt\iftrue}

% Squeeze vertical space around headings for LNCS.
%\setlength{\sectionspace}{0.92\sectionspace}
%\setlength{\subsectionspace}{0.67\subsectionspace}
%\setlength{\subsubsectionspace}{0.50\subsubsectionspace}
%\setlength{\paragraphspace}{0.34\paragraphspace}

% Make lower-level sections smaller for IEEE style
%\renewcommand{\subsection}[1]{\smallskip\noindent{\bfseries{#1.}}}
%\renewcommand{\subsubsection}[1]{\smallskip\noindent{\em #1.}}

% less whitespace between figures and text
%\addtolength\textfloatsep{-4mm}
%\addtolength\floatsep{-1mm}

% Reduce the space around equations
%\addtolength{\belowdisplayskip}{-0.5mm} \addtolength{\belowdisplayshortskip}{-0.5mm}
%\addtolength{\abovedisplayskip}{-0.5mm} \addtolength{\abovedisplayshortskip}{-0.5mm}

% Reduce space before and after verbatim environments
%\preto{\@verbatim}{\topsep=0.4\topsep \partopsep=0.4\partopsep}
% Smaller font size for verbatim environments
%\preto{\@verbatim}{\footnotesize}


%%%%%%%%%% SYMBOLS %%%%%%%%%%%%%%%%%%%
% Checkmark and X that match (http://tex.stackexchange.com/a/42620/16684)
\RequirePackage{pifont}
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}
\RequirePackage{fontawesome}

%%%%%%%%% COMPETITIONS %%%%%%%%%%%%%
\newcommand{\svcomp}{{\small SV-COMP}\xspace}

%%%%%%%%%% UNITS %%%%%%%%%%%%%%%%%%%
\newcommand\GB[1]{\SI{#1}{\giga\byte}}
\newcommand\GHz[1]{\SI{#1}{\giga\hertz}}

%%%%%%%%%% TOOLS %%%%%%%%%%%%%%%%%%%
\newcommand\toolnamesize\smaller
\@ifclassloaded{svjour}{
  \renewcommand\toolnamesize{\relscale{0.9}}
}{
  \@ifclassloaded{svjour3}{\let\toolnamesize\relax}{}
}
\newcommand\tool[1]{{{\toolnamesize\scshape #1}\xspace}}
\newcommand\definetool[2]{\newcommand{#1}{\tool{#2}\xspace}}
\definetool{\blast}       {Blast}
\definetool{\cpachecker}  {CPAchecker}
\definetool{\cbmc}        {Cbmc}
\definetool{\cil}         {Cil}
\definetool{\llvm}        {Llvm}
\definetool{\tvla}        {Tvla}
\definetool{\ocaml}       {OCaml}
\definetool{\tvp}         {Tvp}
\definetool{\camplp}      {CamlP4}
\definetool{\foci}        {Foci}
\definetool{\tcp}         {TCP}
\definetool{\escjava}     {ESC/Java}
\definetool{\slam}        {Slam}
\definetool{\slab}        {Slab}
\definetool{\jpf}         {JPF}
\definetool{\sycmc}       {SyCMC}
\definetool{\impact}      {Impact}
\definetool{\wolverine}   {Wolverine}
\definetool{\ufo}         {Ufo}
\definetool{\java}        {Java}
\definetool{\scratch}     {Scratch}
\definetool{\mathsat}     {MathSAT5}
\definetool{\princess}    {Princess}
\definetool{\smtinterpol} {SMTInterpol}
\definetool{\zthree}      {Z3}
\definetool{\esbmc}       {Esbmc}
\definetool{\pkind}       {PKind}
\definetool{\benchexec}   {BenchExec}
\definetool{\javasmt}     {JavaSMT}
\definetool{\git}         {Git}

\definetool{\twols}       {2ls}
\definetool{\aprove}      {AProVE}
\definetool{\cascade}     {Cascade}
\definetool{\ceagle}      {Ceagle}
\definetool{\ceagleabsref}{Ceagle-Absref}
\definetool{\civl}        {Civl}
\definetool{\cpabam}      {CPA-BAM}
\definetool{\cpakind}     {CPA-kInd}
\definetool{\cparefsel}   {CPA-RefSel}
\definetool{\cpaseq}      {CPA-Seq}
\definetool{\cpawtt}      {CPA-w2t}
\definetool{\divine}      {Divine}
\definetool{\esbmcdepthk} {Esbmc+DepthK}
\definetool{\forest}      {Forest}
\definetool{\forester}    {Forester}
\definetool{\fshellwtt}   {FShell-w2t}
\definetool{\hiprec}      {HIPrec}
\definetool{\impara}      {Impara}
\definetool{\kratos}      {Kratos}
\definetool{\lassoranker} {LassoRanker}
\definetool{\lazycseq}    {Lazy-CSeq}
\definetool{\lctd}        {LCTD}
\definetool{\llbmc}       {Llbmc}
\definetool{\lpi}         {LPI}
\definetool{\mapcheck}    {Map2Check}
\definetool{\mucseq}      {MU-CSeq}
\definetool{\pacman}      {PAC-MAN}
\definetool{\predatorhp}  {PredatorHP}
\definetool{\satabs}      {Satabs}
\definetool{\seahorn}     {SeaHorn}
\definetool{\skink}       {Skink}
\definetool{\smack}       {Smack}
\definetool{\symbiotic}   {Symbiotic}
\definetool{\symdivine}   {SymDIVINE}
\definetool{\terminator}  {Terminator}
\definetool{\uautomizer}  {Ultimate Automizer}
\definetool{\automizer}   {Automizer}
\definetool{\ukojak}      {Ultimate Kojak}
\definetool{\utaipan}     {Ultimate Taipan}
\definetool{\ulcseq}      {UL-CSeq}
\definetool{\vvt}         {Vvt}

\newcommand{\benchexecurl}{https://github.com/sosy-lab/benchexec}
\newcommand{\cpacheckerurl}{https://cpachecker.sosy-lab.org}

%%%%%%%%%% NOTATIONS %%%%%%%%%%%%
\newcommand{\kinduction}{\texorpdfstring{\textit{k}\protect\nobreakdash-induction}{k-induction}\xspace}
\newcommand{\kInduction}{\texorpdfstring{\textit{k}\protect\nobreakdash-Induction}{k-Induction}\xspace}
\newcommand{\CPA}{{CPA}\xspace}
\newcommand{\cegar}{{\ac{CEGAR}}\xspace}
\newcommand{\CFA}{{\ac{CFA}}\xspace}
\newcommand{\ARG}{{\ac{ARG}}\xspace}
\newcommand{\SMT}{{\ac{SMT}}\xspace}
\newcommand{\safe}{{\scshape true}\xspace}
\newcommand{\unsafe}{{\scshape false}\xspace}
\newcommand{\unknown}{{\scshape unknown}\xspace}
\newcommand{\mo}{\text{\small\scshape mo}}
\newcommand\gt{\raisebox{0.3ex}[0pt][0pt]{\smallestfontsize\ensuremath{>}}\,}
\newcommand{\true}{\mathit{true}}
\newcommand{\false}{\mathit{false}}
\newcommand{\seq}[1]{{\langle #1 \rangle}}
\newcommand{\sem}[1]{[\![ #1 ]\!]}
\newcommand{\setsem}[1]{\bigcup_{e \in #1} \sem{e}}
\newcommand{\locs}{\mathit{L}}
\newcommand{\op}{\mathit{op}}
\newcommand{\pc}{\mathit{l}}
\newcommand{\pcvar}{\mathit{l}}
\newcommand{\initial}[1]{#1_0}
\newcommand{\pci}{{\initial\pc}}
\newcommand{\pce}{{\pc_E}}
\newcommand{\target}[1]{#1_{e}}
\newcommand{\pct}{{\target\pc}}
\newcommand{\meet}{\sqcap}
\newcommand{\cpa}{\mathbb{D}}

\newcommand{\concr}{{\cal C}}
\newcommand{\defran}{\textrm{def}}
\newcommand{\varAssignment}{v}
\newcommand{\cpaSymbol}{\cpa}
\newcommand{\interpret}[2]{{#1_{/#2}}}
\newcommand{\eval}[2]{{#1_{/#2}}}
\renewcommand{\path}{\sigma}
\newcommand{\cseq}{{\gamma}}
\newcommand{\cseqintpol}{{\Gamma}}

\newcommand{\conccpa}{\mathbb{C}}
\newcommand{\loccpa}{\mathbb{L}}
\newcommand{\argcpa}{\mathbb{A}}
\newcommand{\locmcpa}{\mathbb{LM}}
\newcommand{\unrollcpa}{\mathbb{R}}
\newcommand{\boundscpa}{\mathbb{LB}}
\newcommand{\predcpa}{\mathbb{P}}
\newcommand{\explcpa}{\mathbb{E}}
\newcommand{\varcpa}{\mathbb{U}}
\newcommand{\defcpa}{\mathbb{RD}}
\newcommand{\constcpa}{\mathbb{CO}}
\newcommand{\aliascpa}{\mathbb{A}}
\newcommand{\shapecpa}{\mathbb{S}}
\newcommand{\compositecpa}{\mathbb{C}}
\newcommand{\assumptionscpa}{\mathbb{A}}
\newcommand{\overflowcpa}{\mathbb{O}}

\newcommand{\cpap}{\text{CPA\raisebox{.2ex}{+}}\xspace}
\newcommand{\cpapa}{\text{CPA\kern-1pt\raisebox{.2ex}{+\!+}}\xspace}

\newcommand{\compcpa}{\mathcal{C}}
\newcommand{\lattice}{\mathcal{E}}
\newcommand{\intlat}{\mathcal{Z}}
\newcommand{\preds}{\mathcal{P}}
\newcommand{\p}{P}

\newcommand{\Nats}{\mathbb{N}}
\newcommand{\Bools}{\mathbb{B}}
\newcommand{\Ints}{\mathbb{Z}}

\newcommand{\less}{\sqsubseteq}
\newcommand{\join}{\sqcup}
\newcommand{\sep}{\mathit{sep}}
\newcommand{\joi}{\mathit{join}}
\newcommand{\strengthen}{\mathord{\downarrow}}
\newcommand{\transconc}[1]{\smash{\stackrel{#1}{\rightarrow}}}
\newcommand{\transabs}[2]{\smash{\stackrel[#2]{#1}{\rightsquigarrow}}}
\newcommand{\mergeop}{\mathsf{merge}}
\newcommand{\merge}{\mergeop}
\newcommand{\stopop}{\mathsf{stop}}
\newcommand{\wait}{\mathsf{waitlist}}
\newcommand{\reached}{\mathsf{reached}}
\newcommand{\result}{\mathsf{result}}
\newcommand{\compare}{\preceq}
\renewcommand{\implies}{\Rightarrow}
\newcommand{\BUG}{{\scshape fa}}
\newcommand{\flag}{\mathit{flag}}
\newcommand{\Itp}[3]{\smash{\mbox{\scshape Itp}{(#2,#3)(#1)}}}

\newcommand{\pre}{\textsf{pre}}

\newcommand{\vars}{X}
\newcommand{\ops}{Ops}
\newcommand{\astate}{e}
\newcommand{\astates}{E}
\newcommand{\PREC}{\Pi}
\newcommand{\precisions}{\Pi}
\newcommand{\programprecisions}{\locs \to 2^\precisions}
\newcommand{\pr}{\pi}
\DeclareMathOperator{\prunion}{%
%\overset{\pr}{\cup}%  Too much height
%\substack{\pr\\[-.10em]\cup}%
{\ooalign{\hidewidth$\pr$\hidewidth\cr$\bigcup$}}%
}
\newcommand{\progpr}{p}
\newcommand{\precfn}{\precop}
\newcommand{\precop}{\mathsf{prec}}
\newcommand{\assumptions}{\Psi}
\newcommand{\as}{\psi}
\newcommand{\stopAlg}{\mathit{break}}
\newcommand{\contAlg}{\mathit{continue}}

\newcommand{\dmid}{\mid}
\newcommand{\pto}{\mbox{$\;\longrightarrow\!\!\!\!\!\!\!\!\circ\;\;\;$}}
%\newcommand{\pto}{\rightharpoonup}
\newcommand{\dom}{\mathit{dom}}

\newcommand\widen{\mathsf{widen}}
%%% Symbol rather?
\newcommand{\refines}{\preccurlyeq}
\newcommand\refine{\mathsf{refine}}
\newcommand\finetune{\mathit{FineTune}}
\newcommand\extractscg{\mathit{ExtractSCG}}
\newcommand\abstr{\mathsf{abstract}}
\newcommand{\off}{\top\hspace{-2.77mm}\bot}

\renewcommand{\hat}{\widehat}
\newcommand{\llbracket}{{[\![}}
\newcommand{\rrbracket}{{]\!]}}
\newcommand{\zug}[1]{{\langle #1 \rangle}}

\newcommand{\SP}[2]{{\mathsf{SP}_{#1}({#2})}}
\newcommand{\WP}[2]{{\mathsf{wp}_{#1}({#2})}}
\newcommand{\blk}{\mathsf{blk}}
\newcommand{\blknever}{\mathsf{blk}^\mathit{never}}
\newcommand{\labs}[2]{{{l^\psi}^{#1}_{\!\!#2}}}
\newcommand{\fcover}{\mathsf{fcover}}
\newcommand{\abs}[3]{(\ensuremath{{#1}})^{#2}_{#3}}
\newcommand{\boolabs}[2]{\abs{#1}{#2}{\mathbb{B}}}
\newcommand{\cartabs}[2]{\abs{#1}{#2}{\mathbb{C}}}
\newcommand{\pf}{\varphi} % path formula
\newcommand{\itp}{\tau} % interpolant
\newcommand{\af}{\tau} % abstract fact

\newcommand{\clv}[1]{\langle#1\rangle}
\newcommand{\eq}[2]{\textsf{eq}.(#1).(#2)}
\newcommand{\closfn}{\textsf{clos*}}
\newcommand{\clos}[3]{\closfn(#1,#2,#3)}
\newcommand{\ite}[3]{\textsf{ite}.#1.#2.#3}
\newcommand{\Subfn}{\textsf{sub}}
\newcommand{\Sub}[2]{\Subfn(#1,#2)}
\newcommand{\Subp}[2]{\textsf{sub}^+(#1,#2)}
\newcommand{\tsize}{\textit{n}}
\newcommand{\inter}[2]{[#1..#2] }
\newcommand{\ident}{\textit{var}}
\newcommand{\fields}{\mathsf{F}}
\newcommand{\field}{\textit{field}}
\newcommand{\mayfn}{\textsf{may}}
\newcommand{\may}[2]{\mayfn(#1,#2)}
\newcommand{\allocated}{\textsf{Alloc}}
\newcommand{\ra}{{\texttt{-\hspace{-.4ex}>}}}
\newcommand{\type}{T}
\newcommand{\alloc}{\textit{alloc}}
\newcommand{\malloc}{\textit{malloc}}
\newcommand{\Bland}{\displaystyle\bigwedge}
\newcommand{\eqvarfn}{\textsf{eqvar}}
\newcommand{\eqvar}[2]{\eqvarfn((#1),(#2))}
\newcommand{\baseof}[1]{\textsf{base}(#1)}
%\newcommand{\asgn}{:\!\!{\scriptstyle =}\,}
\newcommand{\asgn}{\;\texttt{=}\;}
\newcommand{\fieldformula}{\Phi}
\newcommand{\Lib}{{M}}
\newcommand{\enabledscg}{{E}} %{\widehat{M}}
\newcommand{\struct}{\sigma}
\newcommand{\scg}{m}
\newcommand{\trackdef}{D}
\newcommand{\trackdefs}{\mathcal{D}}
\newcommand{\habs}{\Psi}
\newcommand{\habsspec}{\widehat{\Psi}}
\newcommand{\shapetype}{\mathbb{T}}
\newcommand{\shapeclass}{\mathbb{S}}
\newcommand{\shapeclasses}{\mathcal{S}}
\newcommand{\nat}{\mathbb{N}}
\newcommand{\predgen}[1]{P_{#1}}
\newcommand{\predcore}{\predgen{\mathit{core}}}
\newcommand{\predinstr}{\predgen{\mathit{instr}}}
\newcommand{\predabs}{\predgen{\mathit{abs}}}
\newcommand{\predpt}{\predgen{\mathit{pt}}}
\newcommand{\predlab}{\predgen{\mathit{fd}}}
\newcommand{\predall}{\mathcal{P}}
\newcommand{\pred}{\mathit{assume}}
\newcommand{\pt}[1]{\mathit{pt}_{\mathit{#1}}}
\newcommand{\fieldp}[1]{\mathit{fd}_{#1}}
\newcommand{\nullp}[1]{\mathit{np}_{#1}}
\newcommand{\sm}{\textit{sm}}
\newcommand{\equalp}{\textit{eq}}
\newcommand{\reach}[1]{r_{#1}}
\newcommand{\nullpreds}{\mathcal{P}}
\newcommand{\Val}{\textit{val}}
\newcommand{\nodes}{V}
\newcommand{\bop}{\mathrel{\mathit{bop}}}
\newcommand{\PF}{{\mathit{PathFormula}}}
\newcommand{\CONS}{{\mathit{ExtractInterpolants}}}
\newcommand{\oldCONS}{\textsf{Con}}
\newcommand{\genCONS}{\widehat\textsf{Con}}
\newcommand{\Atoms}{\textsf{Atoms}}
\newcommand{\val}{\mathit{val}}
\newcommand{\sel}{\mathsf{sel}}
\newcommand{\upd}{\mathsf{upd}}
\newcommand{\allocpred}{\mathsf{alloc}}

\newcommand{\lval}{\textit{lvalue}}
\newcommand{\command}{\textit{operation}}
\newcommand{\statement}{\textit{statement}}
\newcommand{\predicate}{\textit{predicate}}
\newcommand{\lvexpr}{\textit{expression}}
\newcommand{\cste}{\textit{constant}}
\newcommand{\clean}{\textsf{Clean}}

\newcommand{\depth}{\mathit{depth}}

% label of 'otherwise' edge for witnesses
\newcommand{\otherwise}{\textit{o/w}}
% automaton state labels
\newcommand{\qinit}{q_0}
\newcommand{\qerr}{q_E}
\newcommand{\qbot}{q_\bot}

% k-induction
\newcommand\injectdynamic{{% hack for automatically centering the circle
    \setbox0\hbox{$\longleftarrow$}%
    \rlap{\hbox to \wd0{\hss$\,\pmb\circlearrowleft$\hss}}\box0
}}
\newcommand{\kiki}{KI\injectdynamic{}KI\xspace}
\newcommand{\kidf}{KI\injectdynamic{}DF\xspace}

\endinput
