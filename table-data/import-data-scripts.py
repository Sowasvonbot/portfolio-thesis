import os

prefix = "table-data/filename}"
wdir = os.getcwd()
if "presentation" in wdir:
    prefix = "../" + prefix
prefix = "\\input{" + prefix

script_dir = os.path.dirname(os.path.realpath(__file__))

for file in filter(lambda file: ".tex" in file, os.listdir(script_dir)):
    print(prefix.replace("filename",file))

