\chapter{Introduction}\label{chap:introduction}
Automatic Software Verification is a highly complex branch of computer science. 
Its computation requires a lot of resources and clever algorithms.
That is why there are many different tools to solve such verification tasks.
These tools have different strengths in different situations.
This leads to situations where one tool is able to verify a specific software in under one second and another one needs hours for the same verification.
With a different verification problem the positions may change.
It is therefore highly beneficial to try to combine multiple tools to use each of their strengths.\\[2ex]
For this we contribute a composition, the \portfolio, to always use the fastest verifier.
This composition is created and executed in the program \coveriteam\cite{coveriteam-paper}.
\coveriteam provides its own language for composing multiple tools.
These tools are downloaded and executed by \coveriteam.
The \portfolio takes some tools and executes them in parallel until one tool produces a results, which satisfies a given condition.
Thereupon the other tools will be stopped.
We provide two execution methods for the execution of the \portfolio.
One uses \mpi and the other the multiprocessing module of python.\\[2ex]
For our evaluation we chose a large benchmark set of 8883 verification tasks and use different resource limitations. 
We compared the outcomes to determine which resource limitation impacts the performance at most. 
We have also investigated the performance on a computer cluster.\\[2ex]
%
The goal of this work was to develop a composition, which provides a simple way to combine verification tools into a \portfolio and compare the performance with the state of the art software verification tool \cpachecker. 
\newpage
\paragraph*{Contributions} We make the following contributions:
\begin{enumerate}
\item We provide a new \portfolio composition in the program \coveriteam.
\item We provide two execution methods for this \portfolio, the \mpi execution and the fallback execution. 
\item We implemented a shutdown procedure for the all actors in \coveriteam.
\item We provide a detailed evaluation of the \portfolio composition.
\end{enumerate}
\paragraph*{Impact}
We participated with a \portfolio based verifier in the Software Verification Competition \svcomp 22\cite{SVCOMP22}. 
Our verifier achieved very good results and a high amount of score points. 
However, because it is a combination of already existing tools, it was not included in the ranking of the competition.
Nonetheless, it would have taken 2nd place in many categories in terms of score points.\\[2ex]
Additionally our \portfolio was used in the paper "\textit{Construction of Verifier Combinations Based on Off-the-Shelf Verifiers}" \cite{coveriteam-portfolio-tests}.
In this paper, Beyer, Kanav, and Richter investigated multiple different compositions of verifiers with varying verifier amounts.
The \portfolio achieved the lowest wall time in their evaluation.
\begin{algorithm}[t]
\caption{\portfolio}
\label{alg:portfolio-example}
\algorithmicrequire \quad Program p, Specification s\\
\algorithmicensure \quad Verdict
\begin{algorithmic}[1]
\STATE verifier_1 $\coloneqq$ Verifier("CPAChecker")
\STATE verifier_2 $\coloneqq$ Verifier("ESBMC")
\STATE success_condition $\coloneqq$ verdict $\in \{\textit{TRUE},\,\textit{FALSE}\}$
\STATE parallel_portfolio $\coloneqq$ ParallelPortfolio(verifier_1, verifier_2, success_condition)
\STATE result $\coloneqq$ parallel_portfolio.verify(p,s)
\RETURN (result.verdict,\,result.witness)
\end{algorithmic}
\end{algorithm}
\section*{Running example}
One of the main ideas behind the \portfolio is the combination of strengths of multiple verifiers. 
The two programs \texttt{while_infinite_loop_3.c}
\footnote{\url{
https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/loops/while_infinite_loop_3.c}} and \texttt{sanfoundry_10_ground.c}
\footnote{\url{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/main/c/array-examples/sanfoundry_10_ground.c}} 
located in the \texttt{SV-Benchmark} repository
\footnote{\url{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks}} can be checked against the unreach-call specification.
At the \svcomp 22 \cite{SVCOMP22} both programs were used.
During the competition the verifier \cpachecker\cite{CPACHECKER} was able solve the first program while using 5 seconds of CPU time, but timeouted during the validation of the second program.
The verifier \esbmc\cite{ESBMC-kind} on the other hand was able to solve the second program in less than 1 second of CPU time and timeouted during the validation of the first one (see 
\href{https://sv-comp.sosy-lab.org/2022/results/results-verified/META_ReachSafety.table.html\#/table?filter=id_any(value(sanfoundry_10_ground))&hidden=0,1,2,3,5,6,8,9,10,11,12,13,14,15,16,17,18,19,20}{\blue{\texttt{sanfoundry_10_ground}}} and 
\href{https://sv-comp.sosy-lab.org/2022/results/results-verified/META_ReachSafety.table.html\#/table?filter=id_any(value(while_infinite_loop_3))&hidden=0,1,2,3,5,6,8,9,10,11,12,13,14,15,16,17,18,19,20}
{\blue{\texttt{while_infinite_loop}}} at \svcomp 22). 
Our \portfolio shown in \Cref{alg:portfolio-example} should combine these two verifiers and be able to verify both programs. 