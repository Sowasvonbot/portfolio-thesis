\chapter{Prerequisites}\label{chap:requirements}
This chapter summarizes the technological and conceptual prerequisites to create a parallel portfolio based composition in the program  \coveriteam \cite{coveriteam-paper}. 
\section{Parallel Portfolio}\label{sec:portfolio}
The term portfolio in general describes a collection of multiple things. For example in the economy a portfolio "\textit{refers to any combination of financial assets such as stocks, bonds and cash}" \cite{enwiki:portfolio} . In case of arts, portfolio describes a collection of artworks of an specific artist.
With a portfolio of stocks, you want to minimize the risk of loosing a lot of money, in case one stock is decreasing, therefore you distribute your resources to a lot of stocks. This approach of risk distribution is also usable in computer science.\\[2ex]
%
Huberman et al.\cite{economics-approach} investigated the advantages of portfolios in context of hard computational problems. 
They differentiated between the sequential and the parallel portfolio. Whereas the sequential portfolio has two or more algorithms, which are executed independently and concurrently in sequence, the algorithms of the parallel one where executed in parallel. 
Each of those algorithms is trying to solve the same problem.
They found mainly two advantages with this approach. 
First, the probability of a result was higher than using only one algorithm. 
Second, they achieved a potential performance increase. 
It was especially high in case of the \portfolio.\\[2ex]
Software Verification also includes expensive computations. 
A software verifier calculates several program states and checks them against a certain specification. 
As a result we decided to use a \portfolio to combine multiple software verifiers to achieve better execution time and to increase the probability of a produced result. \\[2ex]
To use a \portfolio, we implemented a new composition in \coveriteam. 
This composition takes a set of verifying tools (see \cref{subsec:actor}), which receive the same input and a success condition (see \cref{par:success-condition}).
Also they must produce the same type of output.
These tools are executed in parallel and the first produced output is checked against this condition.
If it matches the condition, all other tools are stopped, and the result is taken as a result of the whole \portfolio composition.
If no tool produces a valid result, an empty result is returned.
 
\section{\coveriteam}\label{sec:coveriteam}
This section gives a brief overview of \coveriteam. For more detailed information please look at the paper "\textit{CoVeriTeam: On-Demand Composition of Cooperative Verification Systems}" \cite{coveriteam-paper}.

\subsection*{Artifact}\label{subsec:artifact}
An artifact in \coveriteam is a file or some result. 
Artifacts used in this work are:
\begin{itemize}
\item \textbf{Program}: A program, which will be checked against a given specification
\item \textbf{Specification}: A specification, which maybe the program fulfills. For example, is there any line in the program not reached
\item \textbf{Verdict}: True, in  case the specification is fulfilled, false otherwise
\item \textbf{Witness}: Detailed result, how the verdict was achieved.
\item \textbf{TestSuite}: Tests, which cover multiple test cases
\end{itemize}
These Artifacts cover the inputs and outputs for Verifiers and Validators in \coveriteam.

\subsection{Actor}\label{subsec:actor}
Actors act on artifacts. They transform them or create new ones. In other words, an actor is a tool, which will be executed in \coveriteam with artifacts as input and new or changed artifacts as output.
\coveriteam supports three types of actors, the utility actor, atomic actor, and composition.
%
\subsubsection{Atomic actor}\label{subsubsec:atomic-actor}
Atomic actors are external binaries, which are executed by \runexec which is part of the benchmarking framework \benchexec \cite{BenchExec}. 
The command line arguments, download location, allowed resource allocation and version of these programs are provided by \yaml-files. 
\coveriteam saves these tools after downloading them in a cache directory, whose location may be changed by the user.
When it comes to the execution of an atomic actor, \runexec constructs and launches a new process with the help of \cgroups\footnote{\url{https://man7.org/linux/man-pages/man7/cgroups.7.html}}. 
This allows \runexec to limit the resources like CPU time, core amount and memory allocation of this process and measure the actually used resources. After the execution is finished, \runexec collects the produced results and measurements and returns it to \coveriteam.
The produced results are Artifacts (see \cref{subsec:artifact}). 
%
\subsubsection{Compositions}\label{subsubsec:compositions}
A composition is a new actor, which consists of one or (so far) two  actors. Since a composition is a new actor, it is possible create compositions of compositions. At the moment four different compositions are available:
%
\paragraph{Sequence}\label{par:sequence}
A sequential composition does exactly what the title suggests, it executes two actors one after another. Thereby it is important, that the outputs of the first actor are a superset of the inputs of the second one. 
%
\paragraph{Parallel}\label{par:parallel}
This composition executes two actors in parallel. 
It is important, that the inputs do not have a \textit{name clash}. \glqq A name clash between
two sets $A$ and $B$ exists if there is a name in $A$ that is mapped to a different
artifact type in $B$, more formally: $\exists(a, t_{1}) \in A; (a; t_{2}) \in B : t_1 \neq t_2.$\grqq \cite{coveriteam-paper}. The outputs must be disjoint.
%
\paragraph{If-then-else (ITE)}\label{par:ite}
If-then-else requires two actors and a condition. The condition is evaluated at the start of the composition and if it is true, the first actor will be executed. Otherwise the second actor will be executed.
%
\paragraph{Repeat}\label{par:repeat}
This composition only takes one actor and again a condition. The actor is executed repeatedly, until the condition evaluates to true.
%
\subsubsection{Utility actor}\label{subsubsec:utility-actor}
Utility actors manipulate artifacts. For example, they are able to rename or filter keys in Artifacts.
%
\section{Message Passing Interface (\mpi)}\label{sec:mpi}
"\textit{MPI is a standardized and portable message-passing system. Message-passing systems are used especially on distributed machines with separate memory for executing parallel applications. With this system, each executing process will communicate and share its data with others by sending and receiving messages.}"\cite{wiki:mpi}\\[2ex]
%
\mpi\cite{MPICH-paper} defines an interface with methods to share data between processes.
To use \mpi in a program, first one needs to include/import the interface and then compile it with the \mpi-compiler, a modified version of \gcc\footnote{\url{https://gcc.gnu.org/}}. 
To run the compiled program, the developer needs an \mpi implementation on his system and to launch the program with the executable of this implementation.
Because of the specification of \mpi, there must be an executable called \textsc{mpiexec} available. To launch the program, he needs to call \lstinline{mpiexec your_executable}.
\paragraph{Types of inter process communication}
In \mpi several methods for inter process communication are available, however they can be grouped in the following two categories:
\begin{itemize}
\item 1 to 1 communication: One process sends data to one another process
\item collective communication: All processes in a group share data with each other
\end{itemize}
To communicate with each other, processes use communicators. 
A communicator has a unique ID and a group of processes. 
Every process has a unique ID in each communicator, therefore one process can have multiple IDs depending which communicator it is using to transfer data.
To illustrate this, let us consider the following example:
\begin{lstlisting}[language=C, tabsize=4, caption=\texttt{MPI_Send} example,label=lst:basic-mpi-example]
#include <mpi.h>
#include <iostream>

int main()
{
	int my_rank;
	int world_size;

	int test = 0;

	MPI_Init(NULL, NULL);

	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	if (my_rank == 0) { 
		// Only executed in process 0
		int number = 100;
		MPI_Send(&number, 1, MPI_INT, 1, 13, MPI_COMM_WORLD);
	} else if (my_rank == 1) { 
		// Only executed in process 1
		MPI_Recv(&test, 1, MPI_INT, 0, 13, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}
	printf("Process %d has stored number %d in variable test", my_rank, test);

	MPI_Finalize();
	return 0;
}
\end{lstlisting}
After compilation of this code with \textsc{mpicc}, it is executable with the command "\texttt{mpiexec -n 4 program_name}". This is an example of the expected output:
\begin{lstlisting}[caption=Output of \cref{lst:basic-mpi-example}]
Process 1 has stored number 100 in variable test
Process 3 has stored number 0 in variable test
Process 0 has stored number 0 in variable test
Process 2 has stored number 0 in variable test
\end{lstlisting}
Every process is running the same code, the only difference is the value of the \texttt{my_rank} variable. 
This means, only process $0$ sends the number $100$ and only process $1$ receives it, because process $0$ is calling \texttt{MPI_Send} and process $1$ \texttt{MPI_Recv}.
All other processes neither send nor receive a number, so they all output the default value of test, that is 0.
The \texttt{-n} option is used to set the amount of spawned processes. 
However, one can also spawn processes during runtime:
\begin{lstlisting}[language=C, tabsize=4, label=lst:mpi-comm-spawn, caption=\texttt{MPI_Comm_Spawn} example]
#include <mpi.h>
#include <iostream>

int main() {
	int my_rank;
	MPI_Comm interCommunicator;

	MPI_Init(NULL, NULL);

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	printf("Spawner process with rank %d", my_rank);
	MPI_Comm_spawn(
		"mpi_hello_world_win.exe", // The program above
		MPI_ARGV_NULL, 
		3, // Number of spawned processes
		MPI_INFO_NULL, 
		0,
		MPI_COMM_SELF, 
		&interCommunicator, 
		MPI_ERRCODES_IGNORE
	);

	MPI_Finalize();
}
\end{lstlisting}
The output after executing this program with "\texttt{mpiexec -n 1 program_name}" looks like this:
\begin{lstlisting}[caption=Output of \cref{lst:mpi-comm-spawn}]
Spawner process with rank 0
Process 1 has stored number 100 in variable test
Process 2 has stored number 0 in variable test
Process 0 has stored number 0 in variable test
\end{lstlisting}
There are two points to note. First, the process ID $0$ appears two times. Hence the processes spawned with \texttt{MPI_Comm_spawn} have their own world communicator. 
It is possible to address the parent by using \texttt{MPI_Comm_get_parent}. 
This method returns a communicator which includes the spawning process.
Second, it is possible to spawn new processes at runtime, which will come in handy later.