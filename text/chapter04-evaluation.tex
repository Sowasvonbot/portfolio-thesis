\graphicspath{{graphics/benchmarks/}}
\chapter{Evaluation}\label{chap:evaluation}
In this chapter we will have a look at the performance and possibilities of the \portfolio composition. 
In order to perform an evaluation as deep as possible we have limited ourselves to software verifiers as tools inside the \portfolio. 
We therefore used only verification tasks as test data.

\section{Experiment setup}\label{sec:experiment-setup}

\subsection{Verifier selection}\label{sec:tool-selection}
Beyer et al. \cite{coveriteam-portfolio-tests} investigated parts of the \portfolio.
They tested different selections of verifiers from the \svcomp 2021\cite{SVCOMP2021}.
The best performing selection for a \portfolio of three verifiers was the selection of \cpachecker\cite{CPACHECKER}, \esbmc\cite{ESBMC-kind}, and \symbiotic\cite{SYMBIOTIC-SVCOMP21}. 
For the \portfolio of four verifiers, they added \uautomizer\cite{UAUTOMIZER-COMP18}.
We used the same verifiers for our evaluation, therefore every \portfolio of three verifiers in the following experiments used \cpachecker, \esbmc, and \symbiotic. 
With four verifiers we added \uautomizer.

\subsection{Benchmark selection}
For the benchmarks we chose the ReachSafety\footnote{\url{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks}} benchmark set.
It includes 8883 different programs and their results, when checked against the \textbf{unreach call} specification. This specification is a safety property describing that
the error location is never reached \cite{coveriteam-portfolio-tests}.

\subsection{Execution environment}\label{sec:execution-environment}
The benchmarks were executed on the \textsc{verifier cloud}\footnote{\url{https://vcloud.sosy-lab.org/cpachecker/webclient/help/}}. It uses the following specifications:
\begin{itemize}
\item CPU: 3.4 GHz (Intel Xeon E3-1230 v5) with 8 processing units (4 physical + hyper-threading)
\item RAM: 33 GB
\item OS: Ubuntu 20.04
\item MPI-Implementation: \openmpi\footnote{\url{https://www.open-mpi.org//}} 4.0.3  
\end{itemize}
This cloud executes programs inside a \benchexec \cite{BenchExec} container. Therefore, we had the possibility to specify the available resources and measure the actual used resources, like CPU time, wall-time and memory usage.
We used 15 GB as available memory to get a good comparison of the runs of each single verifier at the  \svcomp 2021 \cite{SVCOMP2021}. All \portfolio runs which used the \mpi execution were executed with \openmpi 4.0.3. The only exception is the cluster experiment (see \cref{sec:cluster-run}).
\subsection{Evaluation criteria}
We evaluated the performance of the \portfolio in 5 categories:
\begin{itemize}
\item achieved total score
\item used CPU time
\item used wall-time
\item used memory
\item used energy
\end{itemize}
Because we use the first valid result we expected very good results in terms of wall-time. 
As for CPU time and memory usage higher results were expected.
In each row of the following tables, the best value is marked in bold. 
However, for marking the best value the real numbers are used.
\paragraph{Score} The score is computed with the same schema as used in \svcomp \cite{SVCOMP2021}.
Correct proofs are rewarded with $2$ score points, correct alarms with $1$ score point, wrong proofs with $-32$ score points, and wrong alarms with $-16$ score points.
%
\newpage
\begin{figure}
\centering
\subfloat{\includegraphics[width=0.5\textwidth]{plots/base-comparison-walltime}}
\subfloat{\includegraphics[width=0.5\textwidth]{plots/base-comparison-cputime}}
\caption{Wall-time and CPU time per score point}
\end{figure}
\input{tables/portfolio-base}
\section{General performance}\label{sec:portfolio-base}
For the general performance we used a \portfolio with 3 and 4 verifiers (For verifier selection see \cref{sec:tool-selection}). 
\paragraph*{Resource limitations}
\begin{itemize}
\item CPU-Time limit: 15 minutes
\item Memory limit: 15 GB
\end{itemize}
The same limits were used for the run with \cpachecker.
\paragraph*{Interpretation}
With the score one disadvantage of the \portfolio reveals.
We produce many more correct results than \cpachecker, but also more wrong results.
This was expected, because we use the fastest produced result and we have no way to check, if this result is correct with the standard \portfolio.
We tried to minimize the wrong results with a \portfolio, which uses a combination of a verifier and a validator (see \cref{sec:validating-porfolio}).
But because of the use of the fastest result more verifiers in the \portfolio should increase the score, if we have unlimited resources at our disposal.
With limited resources one may think that there is an optimal ratio of verifier amount and resources. 
Beyer et al. \cite{coveriteam-portfolio-tests} have found a good selection of verifiers for these resource limitations, which we used here also.\\[2ex]
As mentioned in \cref{sec:portfolio}, the main purpose of the \portfolio is to be fast in terms of wall-time, which is proven by the difference in the wall-time for correct results compared with \cpachecker.
In terms of CPU time we expected the outcome to be much higher, because we use more cores at the same time.
The relatively low results can be explained with the low wall-time.
The program does not run long enough per experiment to create high CPU times.
However the memory usage of the \portfolio is higher than of single verifiers.\\[2ex]
%
The most interesting figure is the energy consumption.
As mentioned energy consumption does not scale linearly with other used resources.
This is shown by the \portfolio of four actors. Despite the CPU time and the memory usage is the highest for correct results the energy consumption is the lowest.
\paragraph*{Results}
The \portfolio is fast in terms of wall-time and surprisingly uses less energy than single verifiers. But it also produces more wrong results.
The CPU time is also lower than expected. 
However the total score is not as high as intended, because the wrong results decrease it by a large amount. 
\newpage
%%%%% Main plots
\begin{figure}
\centering
\subfloat{\includegraphics[width=0.5\textwidth]{plots/fallback-comparison-walltime}}
\subfloat{\includegraphics[width=0.5\textwidth]{plots/fallback-comparison-cputime}}
\caption{Wall-time and CPU time per score point}
\label{fig:fallback-compare-plots}
\end{figure}
%%%%%%%
%%%%% Main table
\input{tables/portfolio-fallback}
%%%%%%%
%%%%% Scatter plots
\begin{figure}
\centering
\subfloat[]{\includegraphics[width=0.5\textwidth]{plots/scatter-3-vs-fallback-cputime-log}}
\subfloat[]{\includegraphics[width=0.5\textwidth]{plots/scatter-4-vs-fallback-cputime-log}}\\
\subfloat[]{\includegraphics[width=0.6\textwidth]{plots/scatter-3-vs-fallback-cputime-linear}\label{subref:cputime-linear}}
\caption{Fallback Portfolio vs MPI Portfolio (CPU time)}
\label{fig:fallback-cpu-time-scatter}
\end{figure}
%%%%%%%%%%
\begin{figure}
\centering
\subfloat{\includegraphics[width=0.5\textwidth]{plots/scatter-3-vs-fallback-walltime-log}}
\subfloat{\includegraphics[width=0.5\textwidth]{plots/scatter-4-vs-fallback-walltime-log}}
\caption{Fallback Portfolio vs MPI Portfolio (Wall-time)}
\label{fig:fallback-walltime-scatter}
\end{figure}
%%%%%%%%%%
\section{Fallback vs \mpi execution}\label{sec:fallback-vs-mpi}
To measure the performance differences of the fallback and the \mpi execution, we ran the same benchmarks as in \cref{sec:portfolio-base} with the fallback execution.
\paragraph*{Resource limitations}
\begin{itemize}
\item CPU time limit: 15 minutes
\item Memory limit: 15 GB
\end{itemize}
\paragraph*{Interpretation} 
The most obvious difference is the higher score of the fallback execution.
This is due to the higher amount of correct results, because the wrong result amount is almost the same.\\[2ex]
Another interesting aspect is, while the total CPU time stays about the same, the increased consumption of total wall time.
The explanation for this difference is the in \cref{subsec:entry-in-mpi-environment} described MPI-Scheduler.
This process uses the busy waiting strategy \cite{enwiki:busy-waiting} for waiting at the next result.
Therefore it uses a large amount of CPU time and because we are limiting the CPU time of the benchmarks, the program was stopped earlier.
This effect is shown in \Cref{fig:fallback-cpu-time-scatter} \subref{subref:cputime-linear}.
The difference between the CPU time of the \mpi execution and the fallback execution is constantly increasing, because the MPI-Scheduler uses CPU time while waiting for new results.
Therefore the fallback execution has more total CPU time available for the verifiers. This also explains the higher amount of solved problems.\\[2ex]
Also we noticed the lower median wall- and CPU time with the fallback execution.
We noticed this behavior earlier during the measuring of wall-time with one single program.
\Cref{fig:fallback-compare-plots} and  \Cref{fig:fallback-walltime-scatter} indicate that the creation of an \mpi environment and the spawning of \mpi processes take more time than spawning python processes.
Both fallback executions need less wall-time for the fast tasks when compared with the \mpi execution.
For the heavier tasks the difference becomes less significantly.\\[2ex]
Another advantage of the fallback execution is the slightly lower energy consumption. 
However, this mainly results of the more efficient CPU time usage (see \ref{fig:fallback-cpu-time-scatter}) compared to the \mpi execution. The fallback \portfolio uses the same or less amount of CPU time for almost any verification task.
\paragraph*{Results} 
The fallback execution is faster, more energy efficient and achieves a better score.
It only lacks the possibility of using multiple machines to run the \portfolio.
But used on a single machine, it is the more efficient execution method.
\newpage
\begin{figure}
\centering
\subfloat{\includegraphics[width=0.5\textwidth]{plots/variation-comparison-walltime}}
\subfloat{\includegraphics[width=0.5\textwidth]{plots/variation-comparison-cputime}}
\caption{Wall-time and CPU time per score point}
\end{figure}
\input{tables/3-portfolio-comparison}
\section{Variation of memory and time limit}\label{sec:variate-portfolio}
We wanted to know, which of the two limited resources are more important for the \portfolio. 
In order to investigate this, we have run several different benchmarks.
We decided to use the \portfolio with three verifiers for those benchmarks, because with fewer verifiers each of them benefits more from the increase of memory.
Thus we expected more significant differences. 
\paragraph*{Resource limitations}
\begin{itemize}
\item CPU-Time limit: 15 minutes, if not said otherwise above
\item Memory limit: 15 GB, if not said otherwise above
\item Executed with \mpi
\end{itemize}
\paragraph*{Interpretation} 
As expected, the \portfolio achieved the highest score, when given the most resources. 
However, we did not expect the higher importance of the CPU time limit compared to the memory limit.
Also the increase of the memory limit while using the same CPU time limit had no effect at all.
The seven points difference in the score is due to the fluctuating results.
However, if we increased the CPU time limit, increasing the memory limit began to have a noticeable effect. 
\paragraph*{Results} 
The most limiting factor of the \portfolio is CPU time limit.
Given that we run several tools in parallel, we need significantly more CPU time than running one single verifier.
We used 15 minutes for most of the benchmarks to get a good comparison of single verifiers, because all of those benchmarks were executed with those limits.
\newpage
\begin{figure}
\centering
\subfloat{\includegraphics[width=0.5\textwidth]{plots/validating-comparison-walltime}}
\subfloat{\includegraphics[width=0.5\textwidth]{plots/validating-comparison-cputime}}
\caption{Wall-time and CPU time per score point}
\label{fig:validating-compare-plots}
\end{figure}
\input{tables/validating-portfolio}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{plots/scatter-validating-cputime-true}
\caption{Proof tasks solved by the \texttt{base} \portfolio}
\label{fig:validating-scatter-true}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{plots/scatter-validating-cputime-false}
\caption{Alarms tasks solved by the \texttt{base}  \portfolio}
\label{fig:validating-scatter-false}
\end{figure}
\section{The validating portfolio} \label{sec:validating-porfolio}
As mentioned in \cref{sec:portfolio-base}, the \portfolio produces a lot of wrong results.
We tried to minimize those with this experiment.
The validating \portfolio uses a validator after each verifier, which validates the result of the verifier.
We expected a significantly lower amount of wrong results and tested different resource limitations.
We compared all validating \portfolio experiments with the \portfolio of three verifiers from \Cref{sec:portfolio-base}, which is called \texttt{base} in \cref{table:validating-portfolio}.
\paragraph*{Resource limitations}
\begin{itemize}
\item CPU-Time limit: 15 minutes, if not said otherwise above
\item Memory limit: 15 GB, if not said otherwise above
\item Executed with \mpi
\end{itemize}
\paragraph*{Interpretation}
We have significantly fewer wrong results, but at the expense of performance.
The correct results have been almost halved. 
The remaining 4 wrong alarms are due to the validator.
We cross checked each of those 4 tasks with the results of \svcomp 21 \cite{SVCOMP2021} and the validator produced the same wrong alarms for 3 tasks during the competition. No result was produced for the remaining task in this competition, because the witness listing did not succeed.\\[2ex]
One indication of those poor outcomes is the median used wall-time for correct results.
It is more than doubled compared with the standard \portfolio. It seems, that the validator we used for this benchmarks takes more time on average than the actual verifier.
Additionally the used median CPU time is nearly tripled, which can be explained with the still running verifiers during the validation of a result from an already finished verifier.
For the same reason the median of the used memory is also higher than normal.
All this leads to more than twice the energy consumption.
Interestingly, the total resource consumption is lower than that of the standard \portfolio.\\[2ex]
Another interesting aspect is the low amount of correct proofs (see \cref{table:validating-portfolio}).
While the amount of correct alarms remains about the same the amount of correct proofs is less than half that of the \texttt{base} \portfolio.
We created two scatter plots to investigate this.
\ref{fig:validating-scatter-true} shows all correct proof tasks solved by the \texttt{base} \portfolio.
There are a lot tasks which use very little CPU time but timeout in the validating \portfolio. \Cref{fig:validating-scatter-false} in comparison shows all correct alarm tasks.
Here the amount of timeouting tasks is much lower compared with \Cref{fig:validating-scatter-true}.
Also the used CPU time of the validating \portfolio is much closer to the used CPU time of the \texttt{base} \portfolio.
The only difference between the \texttt{base} and the investigated (15 GB | 15 min) validating \portfolio is the validator after each verifier.
It seems that the validator needs significantly more CPU time in case of a proof than in case of an alarm.
\paragraph*{Results}
The combination of verifier and validator in the \portfolio is very expensive, but it also reduces the produced wrong results to a minimum. And again the increase of CPU time is more beneficial than the increase of the memory limit. 
\newpage
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{plots/cluster-comparison-walltime}
\caption{Wall-time per score point}
\label{fig:cluster-compare-plots}
\end{figure}
\input{tables/cluster-run}
\section{Cluster run}\label{sec:cluster-run}
At last we investigated the performance of the \portfolio when executed on a cluster of multiple PCs. Our test setup consisted of \textbf{four} computers with the following specifications:
\begin{itemize}
\item CPU: 3.4 GHz (Intel i7-6700) with 8 processing units (4 physical + hyper-threading)
\item RAM: 33 GB
\item Network: 1 GBit/s Ethernet
\item OS: Ubuntu 20.04
\item MPI-Implementation: \mpich\footnote{https://www.mpich.org/}
\end{itemize}
Each run had limited resources on the host machine. Since we execute the runs with \runexec we were able to limit the resources only on the machine, which starts the run. 
\paragraph*{Resource limitations}
\begin{itemize}
\item CPU-Time limit: 30 minutes
\item Memory limit: 30 GB (10 GB per verifier)
\item Executed with \mpi
\end{itemize}
\paragraph*{Verifier selection}
As we had significantly more resources at our disposal, we chose all available verifiers from the \textsc{SV-Comp 2021}\footnote{https://sv-comp.sosy-lab.org/2021/} for the \portfolio. Each verifier had a total of 10 GB of memory at its disposal and each machine was running 3 of them.
\paragraph*{Interpretation}
\runexec is capable of measuring the used resources on one machine only, therefore we dropped the CPU time, memory and energy consumption for this experiment. These values would not have been meaningful.\\[2ex]
Since the \portfolio uses the fastest result, we expected a lower wall-time compared to previous runs.
Given a machine with endless resources, the wall-time should always decrease or at least remain the same.
Interestingly the total wall-time for correct results was the same compared with the 4 verifier \portfolio.
This can be explained by the lower verifier count on each machine, therefore the comparsion with the 3 verifier \portfolio is more meaningful here.
The median wall-time decreased significantly compared with both \portfolio, because some of the now included verifiers are much faster in some cases. This is particularly visible in \Cref{fig:cluster-compare-plots}\\[2ex]
Despite being faster, the score of the experiment was disappointing.
Although it solved a lot more verification tasks, it also produced almost double amount of wrong results.
It even falls behind the \portfolio executed with the fallback execution (see \cref{sec:fallback-vs-mpi}).
\paragraph*{Results}
To quadruple the available resources has only led to faster wall-time and a slight increase of score. 
The experiment solved the most problems, but also produced the most errors.
Therefore the results seem to be disappointing. 
But because we used the best combination of verifiers with the previous experiments, there was little room for improvement. 
Our selected verifiers (see \cite{coveriteam-portfolio-tests} for more information) were chosen to complement each other.
This selection took a lot of time.
If this time is not available, a kind of brute force approach is possible. In this case, all verifiers are selected. 
The \portfolio produces promising results even with this approach.
A remaining disadvantage is the high number of false results.
To minimize this number, the validating \portfolio remains to be tested on a cluster (see \cref{sec:future-work}).

\section{Reflection}
\paragraph{The use of \mpi}
At the start of this work we chose \mpi as framework to spawn processes as it provides an easy way to exchange messages between those processes.
These new processes should execute the tools of \coveriteam and send the results back to one specific process. For this result sending we needed the exchange of messages. 
Additionally \mpi allows the execution on PC Cluster, which was another  objective for this work.
However to use \mpi we needed some sort of bridge between the \mpi functions interface, which is only available in C,\,C++, and Fortran.
We chose \mpiforpy (\cite{DALCIN20111124}\cite{DALCIN20051108}\cite{DALCIN2008655}) as bridge. 
Furthermore, we needed an \mpi implementation to execute the program. 
This implementation and \mpiforpy have to be installed on the system and are difficult to ship with \coveriteam. 
Therefore, we chose to implement the fallback execution which only uses features integrated in python.
We then discovered the increased performance of the fallback execution compared to the \mpi execution on a single machine described in \cref{sec:fallback-vs-mpi}.
One could think that the \mpi execution is obsolete now, but in our evaluation we used an already optimized selection of verifiers (see \cref{sec:tool-selection} and \cite{coveriteam-portfolio-tests}).
Without an optimized selection one might want to execute a lot of tools in parallel on multiple machines to get many result with a minimum of preparation time. 
However, the \mpi execution should not be used, when the \portfolio is executed on a single machine. 




