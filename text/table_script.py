from os import replace, stat
import sys

from typing import List
from collections import defaultdict

# run-set_name -> category
run_sets = defaultdict(set)

categories = defaultdict(set)

stati = defaultdict(set)

columns = defaultdict(set)

statistics = defaultdict(dict)

benchmark_names = set()

# Make tree, leaf are the commmands
# Root is run set


preamble = """\\begin{table*}[t]
  \centering
  \caption{your caption}
\setlength{\\tabcolsep}{6pt}
\scalebox{1}{
  \\begin{tabular}{l@{\hskip 10pt}c@{\hskip 12pt}cccc}
    \\toprule
    \multicolumn{1}{l}{\multirow{1}{*}{Benchmarks}}
"""


def read_files(file_paths: list):
    for file in file_paths:
        with open(file=file, mode="r") as f:
            parse_file(f.readlines())

def parse_file(lines: List[str]):
    for line in lines:
        if line.startswith("%"):
            continue
        if line.startswith("\providecommand"):
            continue
        parameters = get_parameters_from_line(line=line)
        parse_parameters(parameters)
    
    
    
    
        
def get_parameters_from_line(line: str) -> List[str]:
    parameters = line.split("}{")
    parameters = [parameter.replace("\StoreBenchExecResult","") for parameter in parameters]
    parameters = [parameter.replace("{","") for parameter in parameters]
    parameters = [parameter.replace("}","") for parameter in parameters]
    parameters = [parameter.replace("\n","") for parameter in parameters]
    return parameters

def parse_parameters(parameters: List[str]):
    if len(parameters) != 7:
        return
    benchmark_name, runset_name, category, status, column_name, statistic, value= parameters
    
    run_sets[runset_name].add(category)
    categories[category].add(status)
    stati[status].add(column_name)
    columns[column_name].add(statistic)
    command = "\\"+"".join(parameters[:-1])
    statistics[statistic][benchmark_name] = command
    
    benchmark_names.add(benchmark_name)
    
    
def to_latex():
    latex_string = ""
    for run_set in run_sets.keys():
    
        latex_string += preamble
        for benchmark in benchmark_names:
            latex_string+= f"\n& \multicolumn{{1}}{{c}}{benchmark}"
        latex_string += "\\\\\n"
        latex_string += "\\midrule\n"
        
        indent = " "
        
        for category in run_sets[run_set]:
            if category in ["Error","Unknown","Wrong"]:
                continue
            latex_string += f"{category}\n\\\\"
            
            for status in categories[category]:
                latex_string += f"{status}\n\\\\"
                
                for column in stati[status]:
                    latex_string += f"{column}\n\\\\"
                    
                    for statistic in columns[column]:
                        pre_string =""
                        if not statistic == "":
                            pre_string = "\\quad"
                        else:
                            pre_string = "Total"
                        latex_string += f"{pre_string} {statistic}\n"
                        
                        for benchmark in benchmark_names:
                            latex_string+= f"& {{\\rndnum{statistics[statistic][benchmark]}}}\n"
                        
                        latex_string+="\\\\\n"
                    latex_string+="\\\\\n"
                latex_string+="\\midrule\n"
            latex_string += "\\midrule\n"
        latex_string = latex_string[:-10]
        latex_string += "\n\\bottomrule\n"
        latex_string +="\\end{tabular}\n"
        latex_string+= "}\n\\end{table*}\n"
                            
    
                                     
    print(latex_string)
    
    
def main():
    read_files(sys.argv[1:])
    to_latex()
    



if __name__ == "__main__":
    main()